import { LightningElement,api,track,wire  } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

import getOpportunityTrialData from '@salesforce/apex/TrialHelper.getOpportunityTrialData';
import createTrials from '@salesforce/apex/TrialHelper.createOpportunityTrials';

import offerTrialHeaderLabel from '@salesforce/label/c.OfferTrialsHeader';
import offerTrialLabel from '@salesforce/label/c.OfferTrial';
import productNameLabel from '@salesforce/label/c.ProductName';
import freeTrialLabel from '@salesforce/label/c.FreeTrial';
import durationLabel from '@salesforce/label/c.Duration';
import noProductLineItemsLabel from '@salesforce/label/c.NoProductLineItems';
import opportunityTrialStageLabel from '@salesforce/label/c.OpportunityMustSetToTrial';
import successfullySavedLabel from '@salesforce/label/c.SuccessfulySaved';
import pleaseSelectProductLabel from '@salesforce/label/c.PleaseSelectProduct';
import cancelLabel from '@salesforce/label/c.Cancel';
import saveLabel from '@salesforce/label/c.Save';

const columns = [
    { label: 'Name', fieldName: 'Name' }
];

export default class OfferTrial extends LightningElement {
    @api recordId;
    @track columns = columns;
 
    @track error;
    @track opportunity;
    @track durationDays = 30;
    @track oppProducts = [];
    @track oppRawData;
    @track saveDisabled = false;

    // Expose the labels to use in the template.
    label = {
        offerTrialHeaderLabel,
        offerTrialLabel,
        productNameLabel,
        freeTrialLabel,
        durationLabel,
        noProductLineItemsLabel,
        opportunityTrialStageLabel,
        successfullySavedLabel,
        pleaseSelectProductLabel,
        cancelLabel,
        saveLabel
    };

    /**
     * @description Query already trialed and avalialbe products.
     */
    @wire(getOpportunityTrialData, {opportunityId : '$recordId'})
    wiredProducts(value) {
        this.oppRawData = value;
        if(value.data) {
            this.error = undefined;
            this.opportunity = JSON.parse(value.data);
            if(this.opportunity === undefined) {
                this.error = this.label.noProductLineItemsLabel;
            } else {
                this.populateOppProducts();
            }
        }
        if(value.error) {
            this.error = value.error;
        }
    }

    /**
     * @description Populate Products available for Trials and Free Trials.
     */
    populateOppProducts() {

        if(this.opportunity) {
            this.error = undefined;
            if(this.opportunity.StageName === 'Trial') {
                if(this.opportunity.OpportunityLineItems && this.opportunity.OpportunityLineItems.records) {
                    for(let i = 0; i < this.opportunity.OpportunityLineItems.records.length; i++) {
                        let product = this.opportunity.OpportunityLineItems.records[i].Product2;
                        product.lineItemId = this.opportunity.OpportunityLineItems.records[i].Id;
                        product.selected = false;
                        this.oppProducts.push(product);
                    }
                } else {
                    this.error = this.label.noProductLineItemsLabel;
                }
            } else {
                //error - Opportunity Stage must be Trial
                this.error = this.label.opportunityTrialStageLabel;
            }
        }
    }
    validateDuration(event) {
        this.durationDays = event.detail.value;
        if(event.detail.value < 1 || event.detail.value > 30) {
            this.saveDisabled = true;
        } else {
            this.saveDisabled = false;
        }
    }
    

    handleCancel () {
        const selectEvent = new CustomEvent('closecustommodal');
        this.dispatchEvent(selectEvent);
    }

     /**
     * @description On selection of 'offer trial' set selected row item values so that
     *              selected products are up-to-date.
     */
    handleRowSelection(event) {
        let prodId = event.target.id.split('-')[0];
        for(let i = 0; i < this.oppProducts.length; i++) {
            if(this.oppProducts[i].Id === prodId) {
                this.oppProducts[i].selected = event.target.checked;
                break;
            }
        }
    }

     /**
     * @description Creates Trial Opportunity, Asset(s) and Opportunity Asset records.
     */
    handleSave() {
        if(this.oppProducts) {
            this.saveDisabled = true;
            let selectedItems = {"duration" : this.durationDays , "items" : []};
            for(let i = 0; i < this.oppProducts.length; i++) {
                if(this.oppProducts[i].selected) {
                    selectedItems.items.push(this.oppProducts[i].lineItemId);
                }
            }
            if(selectedItems.items.length > 0) {
                createTrials({ opportunityId: this.recordId , selectedTrials : JSON.stringify(selectedItems)})
                .then(() => {
                    this.error = undefined;
                    this.showNotification(null, this.label.successfullySavedLabel, 'success');
                    this.handleCancel();
                    return refreshApex(this.oppRawData);
                })
                .catch(error => {
                    this.error = error.body.message;
                    this.showNotification(null, error.body.message, 'error');
                });
            } else {
                this.showNotification(null, this.label.pleaseSelectProductLabel, 'error');
            }
        }
    }

     /**
     * @description Show toast notification.
     */
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
}