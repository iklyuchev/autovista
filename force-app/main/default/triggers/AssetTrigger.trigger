/**
* @author Sapienti7
* @date 08/03/2019
* @description Standard object Asset trigger class
*/
trigger AssetTrigger on Asset (after insert, after update) {
    TriggerDispatcher.mainEntry(TriggerConstant.TRIGGER_NAME_ASSET, 
                                	trigger.isBefore, trigger.isDelete, trigger.isAfter, 
                                	trigger.isInsert, trigger.isUpdate, trigger.isUndelete,
                               		trigger.isExecuting, trigger.new, trigger.newMap, 
                                	trigger.old, trigger.oldMap);
}