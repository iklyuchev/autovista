/**
* @author Sapienti7
* @date 08/03/2019
* @description Standard object Account trigger class
*/
trigger AccountTrigger on Account (before update, after update) {
    TriggerDispatcher.mainEntry(TriggerConstant.TRIGGER_NAME_ACCOUNT, 
                                	trigger.isBefore, trigger.isDelete, trigger.isAfter, 
                                	trigger.isInsert, trigger.isUpdate, trigger.isUndelete,
                               		trigger.isExecuting, trigger.new, trigger.newMap, 
                                	trigger.old, trigger.oldMap);
}