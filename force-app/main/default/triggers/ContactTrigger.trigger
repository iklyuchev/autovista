/**
* @author Sapienti7
* @date 08/03/2019
* @description Standard object Contact trigger class
*/
trigger ContactTrigger on Contact (before delete) {
    TriggerDispatcher.mainEntry(TriggerConstant.TRIGGER_NAME_CONTACT, 
                                	trigger.isBefore, trigger.isDelete, trigger.isAfter, 
                                	trigger.isInsert, trigger.isUpdate, trigger.isUndelete,
                               		trigger.isExecuting, trigger.new, trigger.newMap, 
                                	trigger.old, trigger.oldMap);
}