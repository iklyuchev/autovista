/**
* @author Sapienti7
* @date 10/05/2019
* @description Standard object Opportunity trigger class
*/
trigger OpportunityTrigger on Opportunity (before insert, before update, after update, after insert) {
    TriggerDispatcher.mainEntry(TriggerConstant.TRIGGER_NAME_OPPORTUNITY, 
                                	trigger.isBefore, trigger.isDelete, trigger.isAfter, 
                                	trigger.isInsert, trigger.isUpdate, trigger.isUndelete,
                               		trigger.isExecuting, trigger.new, trigger.newMap, 
                                	trigger.old, trigger.oldMap);
}