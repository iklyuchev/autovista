/**
* @author       Sapienti7
* @date         10/05/2019
* @description  Unit test for AssetGenerationHelper. Tests regeneration of Opportunities and related records on
                Order won opportunity records.
*/
@isTest
private class AssetGenerationHelperTest {

    private static final string OPPORTUNITY_STAGE_WON = System.Label.OpportunityWonStage;

    @testSetup
    private static void setupData() {
        TestDataFactory.insertCustomSettings(false, false, true);
        Account account = TestDataFactory.createAccount(true, 'Test');
        List<Opportunity> opportunities = TestDataFactory.createOpportunities(true, 10, account.Id);

        List<Contact> contacts = TestDataFactory.createContacts(true, 2, account.Id);
        TestDataFactory.createOpportunityContactRoles(true, opportunities, contacts);

        List<Product2> products = TestDataFactory.createProductsWithPriceBook(2, 25.00, false);
        
        List<OpportunityLineItem> lineItems = TestDataFactory.createOpportunityLineItems(true, opportunities, products, 30.00, 2);

        List<Asset> assets = new List<Asset>();
        assets.add(TestDataFactory.createAsset(false, 'Test1', account.Id, products[0].Id,System.today(), 12,false));
        assets.add(TestDataFactory.createAsset(false, 'Test2', account.Id, products[1].Id,System.today(), 12,false));
        
        lineItems = [SELECT Id, Product2Id, Asset_UniqueID__c FROM OpportunityLineItem WHERE Id IN :lineItems];

        Map<String, String> uniqueIdMap = new Map<String, String>();
        for(OpportunityLineItem opplineItem : lineItems) {
            uniqueIdMap.put(opplineItem.Product2Id, opplineItem.Asset_UniqueID__c);        
        }

        for(Asset asset : assets) {
            asset.Unique_Id__c = uniqueIdMap.get(asset.Product2Id);
        }
        insert assets;
        TestDataFactory.createOpportunityAssets(true, opportunities, assets);
    }

    @isTest
    private static void testOnBulkUpdateOpportunities() {
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 50];

        for(Opportunity opp : opps) {
            opp = TestUtils.closeWinOpportunity(opp);
        }
        
        Test.startTest();

        update opps;

        Test.stopTest();

        //Make sure 2 opportunity asset records are created per each Opportunity instance.
        opps = [SELECT Id, StageName, (SELECT Id FROM Opportunity_Assets__r) FROM Opportunity];
        
        for(Opportunity opp : opps) {
            System.assertEquals(2, opp.Opportunity_Assets__r.size(), 'Opportunity Assets are not created on bulk update.');
        }
        System.assertEquals(20, opps.size(), 'There should be double the amount of opps as started with');

        // ensure that only 2 unique Assets are created based on split of Product, Account, and Company
        List<Asset> assets = [SELECT Id, Unique_Id__c, (SELECT Id FROM Opportunity_Assets__r) FROM Asset];
        System.assertEquals(2, assets.size(), 'New Assets for renewal opportunities are not created.');

        for(Asset asset : assets) {
            System.assertEquals(20, asset.Opportunity_Assets__r.size(), 'there should be opportunity assets for every Opportunity');
        }
        
    }

    @isTest
    private static void testAssetSetup(){
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];
        System.assertEquals(2, [SELECT Id FROM Asset].size(), 'Test Assets are not setup correctly.');
        System.assertEquals(10, [SELECT Id FROM Opportunity_Asset__c].size(), 'Test Assets are not setup correctly.');

        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        Opportunity currentOpp = [SELECT Id, (SELECT Id, Asset__c FROM Opportunity_Assets__r) FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        System.assertEquals(1, currentOpp.Opportunity_Assets__r.size(), 'Opportunity Asset test data not setup correctly.');

        Test.startTest();

        update opp;
        
        Test.stopTest();

        Map<Id,Product2> products = new Map<Id,Product2>([SELECT Id, Name FROM Product2]);
        System.assertEquals(2, products.size(), 'Test Product data is not setup correctly.');

        opp = [SELECT Id, (SELECT Id, Asset__c FROM Opportunity_Assets__r) FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        System.assertEquals(2, opp.Opportunity_Assets__r.size(), 'Assets for both products are not created.');
    }

    @isTest
    private static void testAssetFieldsValuesAreInSync() {
        AssetGenerationHelper.setupAssetFieldMappingMetadata();
        Map<String, Set<String>> fields = AssetGenerationHelper.getQueryFields();

        Set<String> oppFieldsSet = fields.get(Schema.SObjectType.Opportunity.getName());
        oppFieldsSet.add('Type');
        Set<String> oppLIFieldsSet = fields.get(Schema.SObjectType.OpportunityLineItem.getName());
        oppLIFieldsSet.add('Asset_UniqueID__c');
        

        List<String> opportunityFields = new List<String>(oppFieldsSet);
        List<String> lineItemFields = new List<String>(oppLIFieldsSet);

        Opportunity opp = Database.query('SELECT Id, '+String.join(opportunityFields, ',')+','
            +' ( SELECT Id, '+String.join(lineItemFields, ',')+' FROM OpportunityLineItems), '
            +' ( SELECT Id, Asset__c, Asset__r.Status, Asset__r.Product2Id FROM Opportunity_Assets__r ) '
            +' FROM Opportunity ORDER BY Name LIMIT 1');

        Test.startTest();

        Asset asset = AssetGenerationHelper.syncAssetFields(null, opp, opp.OpportunityLineItems[0]);

        Test.stopTest();

        for(Asset_Field_Mapping__mdt fieldmapping : AssetGenerationHelper.fieldmappings) {
            String fieldName = fieldmapping.Asset_Field__r.QualifiedApiName;

            Object fieldValue;
            if(fieldmapping.Opportunity_Object__r.QualifiedApiName == Schema.SObjectType.Opportunity.getName()) {
                fieldValue = opp.get(fieldmapping.Opportunity_Field__r.QualifiedApiName);
            } else if(fieldmapping.Opportunity_Object__r.QualifiedApiName == Schema.SObjectType.OpportunityLineItem.getName()){
                fieldValue = opp.OpportunityLineItems[0].get(fieldmapping.Opportunity_Field__r.QualifiedApiName);
            }

            System.assertEquals(asset.get(fieldName), fieldValue, 'Asset field value is not set for '+fieldmapping.Asset_Field__r.QualifiedApiName);

        }

        System.assertEquals(opp.Start_Date__c, asset.Original_Start_Date__c, 'Start Date is not defaulted to Opportunity Start Date.');
    }

    @isTest
    private static void testAssetStartDateIsSetOnActiveAsset() {
        AssetGenerationHelper.setupAssetFieldMappingMetadata();
        Map<String, Set<String>> fields = AssetGenerationHelper.getQueryFields();

        Set<String> oppFieldsSet = fields.get(Schema.SObjectType.Opportunity.getName());
        oppFieldsSet.add('AccountId');
        oppFieldsSet.add('Type');
        Set<String> oppLIFieldsSet = fields.get(Schema.SObjectType.OpportunityLineItem.getName());
        oppLIFieldsSet.add('Asset_UniqueID__c');
        Set<String> assetFieldsSet = fields.get(Schema.SObjectType.Asset.getName());
        assetFieldsSet.add('Status');

        List<String> opportunityFields = new List<String>(oppFieldsSet);
        List<String> lineItemFields = new List<String>(oppLIFieldsSet);
        List<String> assetFields = new List<String>(assetFieldsSet);

        Opportunity opp = Database.query('SELECT Id, '+String.join(opportunityFields, ',')+','
            +' ( SELECT Id, '+String.join(lineItemFields, ',')+' FROM OpportunityLineItems) '
            +' FROM Opportunity ORDER BY Name LIMIT 1');

        Date originalStartDate = System.today().addDays(-2);
        Asset active = TestDataFactory.createAsset(true, 'TEST ASSET', opp.AccountId, opp.OpportunityLineItems[0].Product2Id, originalStartDate, 12, false);
        Id activeId = active.Id;
        String assetQueryStr = String.escapeSingleQuotes('SELECT Id, '+String.join(assetFields, ',')
            + ' FROM Asset WHERE Id = :activeId LIMIT 1');

        active = Database.query(assetQueryStr);
        
        //make sure Asset is active
//        System.assertEquals('Active', active.Status, 'Asset must be active');
        System.assertEquals(originalStartDate, active.Current_Start_Date__c, 'Start Date should be '+originalStartDate);

        Test.startTest();

        Asset asset = AssetGenerationHelper.syncAssetFields(active, opp, opp.OpportunityLineItems[0]);

        Test.stopTest();

        //Asset start date should not be changed as it is active
        System.assertEquals(originalStartDate, asset.Current_Start_Date__c, 'Asset start date should not be changed since it is active.');
    }

    @isTest
    private static void testAssetStartDateIsSetOnInactiveAsset() {
        AssetGenerationHelper.setupAssetFieldMappingMetadata();
        Map<String, Set<String>> fields = AssetGenerationHelper.getQueryFields();

        Set<String> oppFieldsSet = fields.get(Schema.SObjectType.Opportunity.getName());
        oppFieldsSet.add('AccountId');
        oppFieldsSet.add('Type');
        oppFieldsSet.add(Schema.Opportunity.Start_Date__c.getDescribe().getName());
        Set<String> oppLIFieldsSet = fields.get(Schema.SObjectType.OpportunityLineItem.getName());
        oppLIFieldsSet.add('Asset_UniqueID__c');
        Set<String> assetFieldsSet = fields.get(Schema.SObjectType.Asset.getName());
        assetFieldsSet.add('Status');

        List<String> opportunityFields = new List<String>(oppFieldsSet);
        List<String> lineItemFields = new List<String>(oppLIFieldsSet);
        List<String> assetFields = new List<String>(assetFieldsSet);

        Opportunity opp = Database.query('SELECT Id, '+String.join(opportunityFields, ',')+','
            +' ( SELECT Id, '+String.join(lineItemFields, ',')+' FROM OpportunityLineItems) '
            +' FROM Opportunity ORDER BY Name LIMIT 1');

        Date originalStartDate = System.today().addMonths(-24);
        Date opportunityStartDate = System.today();

        Asset inactiveAsset = TestDataFactory.createAsset(true, 'TEST ASSET', opp.AccountId, opp.OpportunityLineItems[0].Product2Id, originalStartDate, 3, false);
        Id inactiveId = inactiveAsset.Id;
        String assetQueryStr = String.escapeSingleQuotes('SELECT Id, '+String.join(assetFields, ',')
            + ' FROM Asset WHERE Id = :inactiveId LIMIT 1');

        inactiveAsset = Database.query(assetQueryStr);
        
        //make sure Asset is inactive
//        System.assertEquals('Inactive', inactiveAsset.Status, 'Asset must be inactive');
        System.assertEquals(originalStartDate, inactiveAsset.Current_Start_Date__c, 'Start Date should be '+originalStartDate);
        System.assertEquals(opportunityStartDate, opp.Start_Date__c, 'Opportunity Start Date should be '+opportunityStartDate);

        Test.startTest();

        Asset asset = AssetGenerationHelper.syncAssetFields(inactiveAsset, opp, opp.OpportunityLineItems[0]);

        Test.stopTest();

        //Asset start date should be changed to the value in the related opportunity.
        System.assertNotEquals(opportunityStartDate, asset.Current_Start_Date__c, 'Start Date should be original start date.');
        System.assertEquals(originalStartDate, asset.Current_Start_Date__c, 'Asset Start Date should be same as the orignial value.');
    }

    @isTest
    private static void testDMLExceptionOnAssetGeneration() {
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];

        for(Opportunity opp : opps) {
            opp = TestUtils.closeWinOpportunity(opp);
        }
        
        Test.startTest();

        AssetGenerationHelper.simulateDMLException = true;
        update opps;

        Test.stopTest();

        //Make sure new assets are not created
        List<Opportunity> oldOpps = [SELECT Id, StageName, (SELECT Id FROM Opportunity_Assets__r) FROM Opportunity WHERE Id IN: opps];
        for(Opportunity opp : oldOpps) {
            System.assertEquals(1, opp.Opportunity_Assets__r.size(), 'New Opportunity Assets should not be created update.');
        }
    }

    @isTest
    private static void testRenewalOpportunityAssetCreation() {
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];
        
        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        Test.startTest();

        update opp;
        
        Test.stopTest();

        Opportunity renewalOpp = [SELECT Id, (SELECT Id, Product2Id FROM OpportunityLineItems), 
            (SELECT Id, Asset__r.Product2Id FROM Opportunity_Assets__r) 
            FROM Opportunity WHERE Parent_Opportunity__c = :opps[0].Id LIMIT 1];
        
        System.assertEquals(2, renewalOpp.OpportunityLineItems.size(), 'Renewal Opportunity Line Items are not created');
        System.assertEquals(2, renewalOpp.Opportunity_Assets__r.size(), 'Renewal Opportunity Assets are not created');
    
        Set<Id> productIds = new Set<Id>();
        for(OpportunityLineItem oppLineItem : renewalOpp.OpportunityLineItems) {
            productIds.add(oppLineItem.Product2Id);
        }
        for(Opportunity_Asset__c oppAsset : renewalOpp.Opportunity_Assets__r) {
            System.assert(productIds.contains(oppAsset.Asset__r.Product2Id), 'Opportunity Asset is not linked to the correct product.');
        }
    }

    @isTest
    private static void testGetMaximumFieldValueNullFieldvalue() {
        Date maximumDate = (Date) AssetGenerationHelper.getMaximumFieldValue(null, System.Today());
        System.assertEquals(System.Today(), maximumDate, 'Maximum date should be set to Today');
    }

    @isTest
    private static void testGetMaximumFieldValueNullAssetFieldvalue() {
        Date maximumDate = (Date) AssetGenerationHelper.getMaximumFieldValue(System.Today(), null);
        System.assertEquals(System.Today(), maximumDate, 'Maximum date should be set to Today');
    }

    @isTest
    private static void testGetMaximumFieldValueLargerAssetFieldvalue() {
        Date maximumDate = (Date) AssetGenerationHelper.getMaximumFieldValue(System.Today(), System.Today().addDays(1));
        System.assertEquals(System.Today().addDays(1), maximumDate, 'Maximum date should be set to Tomorrow');
    }

    @isTest
    private static void testGetMinimumFieldValueNullFieldvalue() {
        Date minimumDate = (Date) AssetGenerationHelper.getMinimumFieldValue(null, System.Today());
        System.assertEquals(System.Today(), minimumDate, 'Minimum date should be set to Today');
    }

    @isTest
    private static void testGetMinimumFieldValueNullAssetFieldvalue() {
        Date minimumDate = (Date) AssetGenerationHelper.getMinimumFieldValue(System.Today(), null);
        System.assertEquals(System.Today(), minimumDate, 'Minimum date should be set to Today');
    }

    @isTest
    private static void testGetMinimumFieldValueLargerAssetFieldvalue() {
        Date minimumDate = (Date) AssetGenerationHelper.getMinimumFieldValue(System.Today(), System.Today().addDays(1));
        System.assertEquals(System.Today(), minimumDate, 'Minimum date should be set to Today');
    }

    @isTest
    private static void testGetSumFieldValueNullFieldvalue() {
        Double sumInt = (Double) AssetGenerationHelper.getSumFieldValue(null, 1);
        System.assertEquals(1, sumInt, 'SumInt should be 1 when added to null');
    }

    @isTest
    private static void testGetSumFieldValueNullAssetFieldvalue() {
        Double sumInt = (Double) AssetGenerationHelper.getSumFieldValue(1, null);
        System.assertEquals(1, sumInt, 'SumInt should be 1 when added to null');
    }

    @isTest
    private static void testGetSumFieldValue() {
        Double sumInt = (Double) AssetGenerationHelper.getSumFieldValue(1, 1);
        System.assertEquals(2, sumInt, 'SumInt should be 2 when added to 1');
    }
}