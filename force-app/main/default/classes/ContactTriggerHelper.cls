/**
* @author Sapienti7
* @date 28/04/2019
* @description TriggerHelper for Contact object.
*/
public with sharing class ContactTriggerHelper {
    /**
    * @author Sapienti7
    * @date 28/04/2019
    * @description Add error on deletion of Primary Contacts.
    */
    public static void validatePrimaryContact(List<Contact> contacts) {
        for(Contact contact : contacts) {
            if(contact.Primary_Contact__c) {
                contact.addError(System.Label.PrimaryContactDeleteError);
            }
        }
    }
}