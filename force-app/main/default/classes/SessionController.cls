/**
* @author Sapienti7
* @date 31/12/2018
* @description Lightining controller class
*/
public with sharing class SessionController {

    @AuraEnabled
    public static String getSessionId() {
        return UserInfo.getSessionId();
    }
}