/**
* @author Sapienti7
* @date 10/05/2019
* @description TriggerHandler for Opportunity object.
*/
public with sharing class OpportunityTriggerHandler extends TriggerHandlerTemplate {

    private static final String CLOSED_WON_STAGE = System.Label.OpportunityWonStage;
    private static final String FIRST_CONTACT_STAGE = System.Label.OpportunityFirstContactStage;
    private static final String CLOSED_LOST_STAGE = System.Label.OpportunityLostStage;
    private static final string OPPORTUNITY_RENEWAL_TYPE = System.Label.OpportunitySalesTypeRenewal;
    public static final string OPPORTUNITY_UPSELL_TYPE = System.Label.OpportunityUpSellType;
    public static final string OPPORTUNITY_NEW_BUSINESS_TYPE = System.Label.OpportunityNewBusinessType;
    public static final string OPPORTUNITY_TRIAL_TYPE = System.Label.OpportunityTrialType;
    
    private static Id trialRecordTypeId;
    private static Boolean renewalCreationIsOff;

    /**
    * @description Overriden method on trigger handler to perform 
                    all the required actions on before insert trigger event
    */
    public override void onBeforeInsert(List<SObject> newlist) {
        
        Set<String> opportunityCountries = new Set<String>();
        List<Opportunity> opportunitiesWithCountries = new List<Opportunity>();
        
        for(Opportunity newOpportunity : (List<Opportunity>)newlist) 
        {
            //if country is populated - get approver from approval matrix.
            if(newOpportunity.Sales_Country__c != null) 
            {
                opportunitiesWithCountries.add(newOpportunity);
                opportunityCountries.add(newOpportunity.Sales_Country__c);
            }      
        }
        OpportunityTriggerHelper.assignApprovers(opportunitiesWithCountries, opportunityCountries);
        OpportunityTriggerHelper.assignPricebooks(opportunitiesWithCountries, opportunityCountries);
    }

    /**
    * @description Overriden method on trigger handler to perform 
                    all the required actions on before update trigger event
    */
    public override void onBeforeUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap) {

        Set<String> opportunityCountries = new Set<String>();

        List<Opportunity> opportunitiesWithCountries = new List<Opportunity>();

        for(Opportunity newOpportunity : (List<Opportunity>)newlist) {
            Opportunity oldOpportunity = (Opportunity)oldMap.get(newOpportunity.Id);

            Boolean assignApprover = (newOpportunity.Sales_Country__c != null && newOpportunity.Sales_Country__c != oldOpportunity.Sales_Country__c);

            //if country is populated - get approver from approval matrix.
            if(assignApprover) {
                opportunitiesWithCountries.add(newOpportunity);
                opportunityCountries.add(newOpportunity.Sales_Country__c);
            }
        }
        OpportunityTriggerHelper.assignApprovers(opportunitiesWithCountries, opportunityCountries);
    }
    
   /**
    * @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on before delete trigger event
    */
    public override void onAfterUpdate(List<SObject> newList, Map<Id, SObject> newMap, List<SObject> oldList, Map<Id, SObject> oldMap){
        
        Map<Id,Opportunity> closedWonOpportunities = new Map<Id, Opportunity>();
        List<Opportunity> closedLostRenewalOpportunities = new List<Opportunity>();
        List<Opportunity> renewalOpportunities = new List<Opportunity>();
        List<Opportunity> closedWonUpsellOpportunities = new List<Opportunity>();
        List<Opportunity> statusChangedOpportunities = new List<Opportunity>();
        
        for(Opportunity opp : (List<Opportunity>)newList) {
            Opportunity oldOpportunity = (Opportunity)oldMap.get(opp.Id);
            Boolean isOppStageClosedWon = (oldOpportunity.StageName != CLOSED_WON_STAGE
                                            && opp.StageName == CLOSED_WON_STAGE && !opp.Closed_Won_First_Time__c);

            if(isOppStageClosedWon) {
                closedWonOpportunities.put(opp.Id, opp);
                Boolean isOppRenewable = (opp.Type != OPPORTUNITY_UPSELL_TYPE)
                    && (opp.Type != OPPORTUNITY_TRIAL_TYPE && opp.RecordTypeId != getTrialRecordTypeId());
                Boolean isUpsell = (opp.Type == OPPORTUNITY_UPSELL_TYPE);
                if(isOppRenewable) {
                    renewalOpportunities.add(opp);
                } else if(isUpsell) {
                    closedWonUpsellOpportunities.add(opp);
                }
            }

            Boolean isRenewalOppStageClosedLost = oldOpportunity.StageName != CLOSED_LOST_STAGE
                                            && opp.StageName == CLOSED_LOST_STAGE
                                            && opp.Type == OPPORTUNITY_RENEWAL_TYPE;

            if(isRenewalOppStageClosedLost) {
                closedLostRenewalOpportunities.add(opp);
            }

            Boolean isStageChanged = opp.StageName != FIRST_CONTACT_STAGE
                                        &&  opp.StageName != CLOSED_LOST_STAGE
                                        && oldOpportunity.StageName != opp.StageName
                                        && (opp.RecordTypeId == null || opp.RecordTypeId != getTrialRecordTypeId());
            if(isStageChanged){
                statusChangedOpportunities.add(opp);
            }
        }
        if(statusChangedOpportunities.size() > 0) {
            OpportunityTriggerHelper.validatePrimaryContact(statusChangedOpportunities);
        }

        if(closedWonOpportunities.size() > 0 || closedLostRenewalOpportunities.size() > 0) {
            opportunityClosedActions(renewalOpportunities, closedWonOpportunities, closedLostRenewalOpportunities);
        }
        if(closedWonUpsellOpportunities.size() > 0) {
            OpportunityTriggerHelper.updateRenewalOpportunities(closedWonUpsellOpportunities);
        }
    }

    /**
    * @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after insert of Opportunities.
    */
    public override void onAfterInsert(List<SObject> newlist, Map<Id, SObject> newmap){

        Map<Id,Opportunity> parentOpportunities = new Map<Id,Opportunity>();
        List<Opportunity> closedWonOpportunities = new List<Opportunity>();
        Set<String> allProductCodes = new Set<String>();
        Map<Id, Opportunity> trialOpportunitiesMap = new Map<Id, Opportunity>();

        for(Opportunity opp : (List<Opportunity>)newList) {
            if(opp.Product_Code__c != null) {
                parentOpportunities.put(opp.Id, opp);
                allProductCodes.add(opp.Product_Code__c);
            }

            Boolean closedWon = opp.StageName == CLOSED_WON_STAGE
                                && (opp.RecordTypeId == null || opp.RecordTypeId != getTrialRecordTypeId());
            if(closedWon){
                closedWonOpportunities.add(opp);
            }
            
            
            if(opp.RecordTypeId == getTrialRecordTypeId())
            {
                trialOpportunitiesMap.put(opp.Parent_Opportunity__c, opp);
            }
        }

        if(parentOpportunities.size() > 0 && allProductCodes.size() > 0) {
            OpportunityTriggerHelper.createTrialOpportunities(parentOpportunities, allProductCodes);
        }

        if(closedWonOpportunities.size() > 0) {
            OpportunityTriggerHelper.validatePrimaryContact(closedWonOpportunities);
        }
        
        if(trialOpportunitiesMap.size() > 0)
        {
            TrialHelper.createTrialContactRoles(trialOpportunitiesMap);
        }
    }

    protected override Boolean getTriggerIsOff() {
        if(triggerIsOff == null) {
            Bypass_Configuration__c bypassConfig = Bypass_Configuration__c.getInstance();
            triggerIsOff = bypassConfig == null ? false : bypassConfig.IsTriggerFrameworkBypassed__c && !bypassConfig.IsOpportunityTriggerEnabled__c;
        }
        return triggerIsOff;
    }

    private static Boolean getRenewalCreationIsOff() {
        if(renewalCreationIsOff == null) {
            Bypass_Configuration__c bypassConfig = Bypass_Configuration__c.getInstance();
            renewalCreationIsOff = bypassConfig == null ? false : bypassConfig.IsRenewalOppCreationBypassed__c;
        }
        return renewalCreationIsOff;
    }
    /**
    * @description Actions to be executed for Closed Won Opportunities.
    */
    private static void opportunityClosedActions(List<Opportunity> renewalOpportunities, Map<Id, Opportunity> closedWonOpportunities, List<Opportunity> closedLostRenewalOpportunities) {

        //Regenerate new opportunties on closed won
        if(!getRenewalCreationIsOff()) {
            OpportunityTriggerHelper.createNewRenewalOpportunities(renewalOpportunities);
        }
        
        
        //Generate assets for closed won opportunities.
        AssetGenerationHelper.generateAssetsAndRemoveGracePeriods(closedWonOpportunities, closedLostRenewalOpportunities);

    }

    private static Id getTrialRecordTypeId() {
        
        if(trialRecordTypeId == null) {
            if(Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().containsKey('Trial')) {
                trialRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Trial').getRecordTypeId();
            }
        }
        return trialRecordTypeId;
    }
}