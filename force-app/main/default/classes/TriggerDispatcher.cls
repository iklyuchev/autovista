/**
* @author Sapienti7
* @date 20/12/2018
* @description This class holds the functionality to pass trigger events into trigger framework.
*/
public without sharing class TriggerDispatcher {

    //static variable to hold the base class to the trigger controller
    public static ITriggerController activeFunction = null;
    public static String objectName = null;

    /**
    * @author Sapienti7
    * @date 20/12/2018
    * @description This static method should be called in all triggers to handle dedicated trigger handlers execution.
    */
    public static void mainEntry(String triggerObject, Boolean isBefore, 
                                 Boolean isDelete, Boolean isAfter,
                                 Boolean isInsert, Boolean isUpdate, 
                                 Boolean isUndelete, Boolean isExecuting,
                                 List<SObject> newlist, Map<Id, SObject> newmap,
                                 List<SObject> oldlist, Map<Id, SObject> oldmap){
       
        triggerObject = triggerObject + TriggerConstant.TRIGGER_HANDLER_STRING;
   
        if(activeFunction == null || triggerObject != objectName){
            objectName = triggerObject;
            Type tHandler = Type.forName(triggerObject);

            try{
                activeFunction = (ITriggerController)tHandler.newInstance();
            } catch(NullPointerException e) {
                Log.push('Trigger Dispatcher');
                Log.error('This exception may be related to a null pointer', e);
                Log.emit();
            }
            
            activeFunction.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap); 
        } else {
            activeFunction.inProgressEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
        }
    }
}