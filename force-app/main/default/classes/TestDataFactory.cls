/**
* @author Sapienti7
* @date 28/04/2019
* @description Test Data Factory for unit tests.
*/
@IsTest
public class TestDataFactory {
    
    public static void insertCustomSettings(Boolean isBypass) {
        Bypass_Configuration__c bypassConfig = new Bypass_Configuration__c(IsTriggerFrameworkBypassed__c = isBypass);
        insert bypassConfig;
    }

    public static void insertCustomSettings(Boolean isBypass, Boolean isOpportunityEnabled) {
        Bypass_Configuration__c bypassConfig = new Bypass_Configuration__c(IsTriggerFrameworkBypassed__c = isBypass, IsOpportunityTriggerEnabled__c = isOpportunityEnabled);
        insert bypassConfig;
    }

    public static void insertCustomSettings(Boolean isTriggerBypassed, Boolean isOpportunityEnabled, Boolean isValidationBypassed) {
        Bypass_Configuration__c bypassConfig = new Bypass_Configuration__c(
            IsTriggerFrameworkBypassed__c = isTriggerBypassed, 
            IsOpportunityTriggerEnabled__c = isOpportunityEnabled,
            IsValidationBypassed__c = isValidationBypassed
        );
        insert bypassConfig;
    }


    public static Account createAccount(Boolean insertToDatabase, String accountName) {
        Account account = createAccount(insertToDatabase, accountName, 'Test Street', 'Test City');
        return account;
    }

    public static Account createAccount(Boolean insertToDatabase, String accountName, String streetName, String cityName) {
        Account account = new Account(Name = accountName, BillingStreet = streetName, BillingCity = cityName, BillingPostalCode ='TE 5TA', BillingCountry = 'United Kingdom',
            Industry_Segment__c = 'Finance', Industry_Sub_Segment__c = 'Captive Finance House');
        if(insertToDatabase) {
            insert account;
        }
        return account;
    }

    public static List<Account> createAccounts(Boolean insertToDatabase, Integer numberOfAccounts) {

        List<Account> accounts = new List<Account>();

        for(Integer i = 0; i < numberOfAccounts; i++) {
            accounts.add(createAccount(false, 'Test '+i, 'Test Street'+i, 'Test city'+i));
        }

        if(insertToDatabase) {
            insert accounts;
        }
        return accounts;
    }

    public static Contact createContact(Boolean insertToDatabase, Id accountId, String firstName, String lastName, String emailAddress) {
        Contact contact = new Contact(AccountId = accountId, FirstName = firstName, LastName = lastName, Email = emailAddress);
        
        if(insertToDatabase) {
            insert contact;
        }
        return contact;
    }

    public static List<Contact> createContacts(Boolean insertToDatabase, Integer numberOfContacts, Id accountId) {
        List<Contact> contacts = new List<Contact>();

        for(Integer i=0; i<numberOfContacts; i++) {
            contacts.add(createContact(false, accountId, 'TEST '+i, 'TESTER '+i, 'tester'+i+'@test.com'));
        }
        if(insertToDatabase) {
            insert contacts;
        }
        return contacts;
    }

    public static Opportunity createOpportunity(Boolean insertToDatabase, String name, Id accountId, String stageName, Date closeDate, String country, Integer durationMonths, Boolean isTrial) {
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.Type = OpportunityTriggerHandler.OPPORTUNITY_NEW_BUSINESS_TYPE;
        opp.Pricebook2Id = Test.getStandardPricebookId();
        opp.AccountId = accountId;
        opp.StageName = stageName;
        opp.CloseDate = closeDate;
        opp.Sales_Country__c = country;
        opp.Start_Date__c = System.today();
        opp.End_Date__c = System.today().addMonths(durationMonths);

        if(isTrial) {
            opp.RecordTypeId = TrialHelper.trialRecordTypeId;
        }

        if(insertToDatabase) {
            insert opp;
        }
        return opp;
    }

    public static List<Opportunity> createOpportunities(Boolean insertToDatabase, Integer numberOfOpportunities, Id accountId) {
        List<Opportunity> opportunities = new List<Opportunity>();

        for(Integer i=0; i<numberOfOpportunities; i++) {
            opportunities.add(createOpportunity(false, 'TEST '+i, accountId, System.Label.OpportunitySalesQualifiedStage, Date.today().addDays(30), 'United Kingdom', 12, false));
        }

        if(insertToDatabase) {
            insert opportunities;
        }
        return opportunities;
    }

    public static OpportunityContactRole createOpportunityContactRole(Boolean insertToDatabase, Id contactId, Id opportunityId, String role, Boolean isPrimary) {
        OpportunityContactRole opc = new OpportunityContactRole(ContactId = contactId, OpportunityId = opportunityId, Role = role, IsPrimary = isPrimary);

        if(insertToDatabase) {
            insert opc;
        }
        return opc;
    } 

    public static List<OpportunityContactRole> createOpportunityContactRoles(Boolean insertToDatabase, List<Opportunity> opportunities, List<Contact> contacts) {
        List<OpportunityContactRole> opcs = new List<OpportunityContactRole>();

        for(Opportunity opportunity : opportunities) {
            for(Integer i = 0;i < contacts.size(); i++) {
                opcs.add(createOpportunityContactRole(false, contacts[i].Id, opportunity.Id, 'Test', i == 0));
            }
        }

        if(insertToDatabase) {
            insert opcs;
        }
        return opcs;
    }

    public static Asset createAsset(Boolean insertToDatabase, String assetName, Id accountId, Id productId, Date startDate, Integer durationMonths, Boolean isTrial, Integer gracePeriod) {
        Asset asset = new Asset(Name = assetName, 
            AccountId = accountId, 
            Product2Id = productId, 
            Current_Start_Date__c = startDate, 
            Current_End_Date__c = startDate.addMonths(durationMonths), 
            Trial__c = isTrial,
            Status = 'Active',
            Grace_Period__c = gracePeriod);

        if(insertToDatabase) {
            insert asset;
        }
        return asset;
    }

    public static Asset createAsset(Boolean insertToDatabase, String assetName, Id accountId, Id productId, Date startDate, Integer durationMonths, Boolean isTrial) {
        Asset asset = createAsset(
            insertToDatabase,
            assetName, 
            accountId, 
            productId, 
            startDate, 
            durationMonths, 
            isTrial,
            0
        );
        return asset;
    }

    public static List<Asset> createAssets(Boolean insertToDatabase, Integer numberOfAssets, Id accountId, Id productId, Date startDate, Integer durationMonths, Boolean isTrial, Integer gracePeriod) {
        List<Asset> assets = new List<Asset>();

        for(Integer i = 0; i < numberOfAssets; i++) {
            assets.add(createAsset(false, 'Test '+i, accountId, productId, startDate, durationMonths, isTrial, gracePeriod));
        }
        if(insertToDatabase) {
            insert assets;
        }
        return assets;
    }

    public static List<Asset> createAssets(Boolean insertToDatabase, Integer numberOfAssets, Id accountId, Id productId, Date startDate, Integer durationMonths, Boolean isTrial) {
        List<Asset> assets = new List<Asset>();

        for(Integer i = 0; i < numberOfAssets; i++) {
            assets.add(createAsset(false, 'Test '+i, accountId, productId, startDate, durationMonths, isTrial));
        }
        if(insertToDatabase) {
            insert assets;
        }
        return assets;
    }

    public static List<Opportunity_Asset__c> createOpportunityAssets(Boolean insertToDatabase, List<Opportunity> opportunities, List<Asset> assets) {
        List<Opportunity_Asset__c> oppAssets = new List<Opportunity_Asset__c>();

        for(Integer i = 0; i < opportunities.size(); i++) {
            Integer assetSize = assets.size();
            Id assetId = assets[Math.mod(i, assetSize)].Id;
            oppAssets.add(new Opportunity_Asset__c(Asset__c = assetId, Opportunity__c = opportunities[i].Id));
        }
        if(insertToDatabase) {
            insert oppAssets;
        }
        return oppAssets;
    }

    public static Pricebook2 activateStandardPricebook(){
        // Instantiate the Pricebook2 record first, setting the Id
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );

        update standardPricebook;
        
        standardPricebook = [ SELECT IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id ];
        System.assertEquals(true, standardPricebook.IsStandard, 'The Standard Pricebook should now return IsStandard = true');
        return standardPricebook;
    }

    public static Pricebook2 createPricebook(Boolean insertToDatabase, String name, String country) {
        PriceBook2 pb = new PriceBook2(
            Name = name,
            IsActive = true,
            Country__c = country
        );
        
        if(insertToDatabase) {
            insert pb;
        }
        return pb;
    }

    public static Product2 createProduct(Boolean insertToDatabase, String name, String productCode, Boolean isFreeTrial) {
        Product2 testProduct = new Product2(
            Name = name,
            IsActive = true,
            ProductCode = productCode,
            Is_Free_Trial__c = isFreeTrial
        );

        if(insertToDatabase) {
            insert testProduct;
        }
        return testProduct;
    }

    public static Product2 createProductWithPriceBook(String productName, String productCode, Decimal unitPrice, Boolean isTrial){
        
        Product2 testProduct = createProduct(true, productName, productCode, isTrial);
        List<PricebookEntry> priceBookEntriesList = new List<PricebookEntry>();
        
        priceBookEntriesList.add(new PricebookEntry(
            IsActive = true,
            UseStandardPrice = false,
            UnitPrice = unitPrice,
            Product2Id = testProduct.Id,
            Pricebook2Id = Test.getStandardPricebookId()
        ));

        insert priceBookEntriesList;

        return testProduct;
    }

    public static List<Product2> createProductsWithPriceBook(Integer numberOfProducts, Decimal unitPrice, Boolean isTrial){
        
        List<Product2> products = new List<Product2>();
        for(Integer i =0;i < numberOfProducts; i++) {
            products.add(createProduct(false, 'TEST Product '+i, 'Test '+i, isTrial));
        }

        insert products;

        List<PricebookEntry> priceBookEntriesList = new List<PricebookEntry>();
        
        for(Integer i = 0; i < numberOfProducts; i++) {
            priceBookEntriesList.add(new PricebookEntry(
                IsActive = true,
                UseStandardPrice = false,
                UnitPrice = unitPrice,
                Product2Id = products.get(i).Id,
                Pricebook2Id = Test.getStandardPricebookId()
            ));
        }
        
        insert priceBookEntriesList;

        return products;
    }

    public static List<OpportunityLineItem> createOpportunityLineItems(Boolean insertToDatabase, List<Opportunity> opportunities, List<Product2> products, Decimal unitPrice, Integer quantity) {
        
        List<OpportunityLineItem> oplineItems = new List<OpportunityLineItem>();

        for(Opportunity opportunity : opportunities) {
            for(Product2 p : products) {
                oplineItems.add(createOpportunityLineItem(false, opportunity.Id, p.Id, unitPrice, quantity));
            }
        }

        if(insertToDatabase) {
            insert oplineItems;
        }
        
        return oplineItems;
    }

    public static OpportunityLineItem createOpportunityLineItem(Boolean insertToDatabase, Id opportunityId, Id productId, Decimal unitPrice, Integer quantitiy) {

        OpportunityLineItem opli = new OpportunityLineItem(OpportunityId = opportunityId, Product2Id = productId, UnitPrice = unitPrice, Quantity = quantitiy, Grace_Period__c = 30, Revenue_Type__c = System.Label.RevenueTypeSubscription);

        if(insertToDatabase) {
            insert opli;
        }
        return opli;
    }

    public static Integration_Log__c createIntegrationLog(Boolean insertToDatabase,  String integraion, String targetId, String status, String targetObject) {
        
        Integration_Log__c il = new Integration_Log__c(Integration__c = integraion, Status__c = status, Target_Object__c = targetObject, Target_Id__c = targetId);
        if(insertToDatabase) {
            insert il;
        }
        return il;
    }

    public static List<Integration_Log__c> createIntegrationLogs(Boolean insertToDatabase,  String integraion, Set<Id> targetIds, String status, String targetObject) {
        List<Integration_Log__c> logs = new List<Integration_Log__c>();
        for(Id targetId : targetIds) {
            Integration_Log__c il = new Integration_Log__c(Integration__c = integraion, Status__c = status, Target_Object__c = targetObject, Target_Id__c = targetId);
            logs.add(il);
        }
       
        if(insertToDatabase) {
            insert logs;
        }
        return logs;
    }
    
    public static User createUser(Boolean insertToDatabase, String profileName, String emailAddress, String userNameString){
        List<Profile> p = [SELECT id FROM Profile WHERE name = :profileName];
        Id queriedProfileId = p[0].id;
        User u = new User(alias = 'avxxxzzz', email=emailAddress,
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = queriedProfileId,
            timezonesidkey='America/Los_Angeles', username=userNameString);

        if(insertToDatabase) {
            insert u;
        }
        return u;
    }

    public static Cloned_Object_Fields_Default_Value__mdt getDefaultMetadataField(String objectName, String fieldName) {
        for(Cloned_Object_Fields_Default_Value__mdt mdt : SObjectCloner.queryCustomMetadataTypes(System.Label.OpportunityCloningProcess, objectName)) {
            if(mdt.Cloned_Field__r.QualifiedApiName == fieldName) {
                return mdt;
            }
        }
        return null;
    } 

    public static List<Opportunity_Approvers__mdt> getApproverMetaData(User approver) {
        String approverMetaData = '[{"attributes":{"type":"Opportunity_Approvers__mdt"},"Id":"m021x0000004jbUAAQ","Country__c":"United Kingdom","Country_Director_Approver__c":"'+approver.Id+'","Regional_Director_Approver__c":"'+approver.Id+'"}]';
        List<Opportunity_Approvers__mdt> approversMetadata = (List<Opportunity_Approvers__mdt>)JSON.deserialize(approverMetaData, List<Opportunity_Approvers__mdt>.class);

        return approversMetadata;
    }
}