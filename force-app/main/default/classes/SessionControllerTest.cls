/**
* @author Sapienti7
* @date 31/12/2018
* @description Test class for SessionController apex class
*/
@isTest
private class SessionControllerTest {
    @isTest static void getSessionIdTest(){
        System.assertEquals(UserInfo.getSessionId(), SessionController.getSessionId(), 'Expected to get current user session id');
    } 
}