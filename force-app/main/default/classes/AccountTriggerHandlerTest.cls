/**
* @author 		Sapienti7
* @date 		11/07/2019
* @description  Unit test for AccountTriggerHandler. Tests creation of Integration logs on Updating Accounts.
*/
@isTest
private class AccountTriggerHandlerTest {

    @testSetup
	private static void setupData() {

        TestDataFactory.insertCustomSettings(true, false, true);

		List<Account> accounts = TestDataFactory.createAccounts(true, 50);
        List<Product2> products = TestDataFactory.createProductsWithPriceBook(1, 25.00, false);

        List<Asset> assets = new List<Asset>();

        for(Account account : accounts) {
            Asset a = TestDataFactory.createAsset(false, 'test '+account.Id, account.Id, products[0].Id, System.today(), 12, false);
            assets.add(a);
        }

        insert assets;
	}

    @isTest
    private static void testOnAccountUpdateWithoutBypass() {
        List<Account> accounts = [SELECT Id FROM Account];
        Set<Id> accountIds = new Set<Id>();

        IntegrationHelper.accountSyncFields = new Set<String>{'Name'};

        for(Account a : accounts) {
            a.Name = 'TEST ACCOUNT NAME UPDATED';
            accountIds.add(a.Id);
        }

        List<Bypass_Configuration__c> bypassConfig = [SELECT Id FROM Bypass_Configuration__c];
        delete bypassConfig;

        Test.startTest();

        update accounts;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, Integration__c, Status__c, Target_Object__c, Target_Id__c FROM Integration_Log__c WHERE Target_Id__c IN : accountIds];
        System.assertEquals(50, logs.size(), 'Integration logs are not created for all the updated accounts');
        
        for(Integration_Log__c log : logs) {
            System.assertEquals(IntegrationHelper.INTEG_LOG_INTEGRATION, log.Integration__c, 'Invalid Integration is set on log '+ log.Integration__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_NEW_STATUS, log.Status__c, 'Invalid Status is set on log '+ log.Status__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_TARGET_OBJECT, log.Target_Object__c, 'Invalid Target Object is set on log '+ log.Target_Object__c);
        }
    }

    @isTest
    private static void testOnAccountUpdateWithBypass() {
        List<Account> accounts = [SELECT Id FROM Account];
        Set<Id> accountIds = new Set<Id>();

        IntegrationHelper.accountSyncFields = new Set<String>{'Name'};

        for(Account a : accounts) {
            a.Bypass_Bulk_Upload__c = true;
            a.Name = 'TEST ACCOUNT NAME UPDATED';
            accountIds.add(a.Id);
        }

        List<Bypass_Configuration__c> bypassConfig = [SELECT Id FROM Bypass_Configuration__c];
        delete bypassConfig;

        Test.startTest();

        update accounts;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, Integration__c, Status__c, Target_Object__c, Target_Id__c FROM Integration_Log__c WHERE Target_Id__c IN : accountIds];
        System.assertEquals(0, logs.size(), 'Integration logs should not created for updated accounts');
    }

    @isTest
    private static void testWithExistingIntegrationLogs() {
        List<Account> accounts = [SELECT Id FROM Account LIMIT 10];

        List<Integration_Log__c> logs = new List<Integration_Log__c>();
        for(Account acc : accounts) {
            Integration_Log__c log = TestDataFactory.createIntegrationLog(
                false, 
                IntegrationHelper.INTEG_LOG_INTEGRATION, 
                acc.Id, IntegrationHelper.INTEG_LOG_NEW_STATUS, 
                IntegrationHelper.INTEG_LOG_TARGET_OBJECT);
                
            logs.add(log);
        }

        insert logs;

        IntegrationHelper.accountSyncFields = new Set<String>{'Name'};

        List<Account> allAccounts = [SELECT Id FROM Account];
        for(Account a : allAccounts) {
            a.Name = 'TEST ACCOUNT NAME UPDATED';
        }

        List<Bypass_Configuration__c> bypassConfig = [SELECT Id FROM Bypass_Configuration__c];
        delete bypassConfig;

        Test.startTest();

        update allAccounts;

        Test.stopTest();

        logs = [SELECT Id, Integration__c, Status__c, Target_Object__c, Target_Id__c FROM Integration_Log__c WHERE Id NOT IN : logs];
        System.assertEquals(40, logs.size(), 'Integration logs are not created for all the updated accounts');
        
        for(Integration_Log__c log : logs) {
            System.assertEquals(IntegrationHelper.INTEG_LOG_INTEGRATION, log.Integration__c, 'Invalid Integration is set on log '+ log.Integration__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_NEW_STATUS, log.Status__c, 'Invalid Status is set on log '+ log.Status__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_TARGET_OBJECT, log.Target_Object__c, 'Invalid Target Object is set on log '+ log.Target_Object__c);
        }
    }
}