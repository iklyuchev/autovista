/**
* @author Sapienti7
* @date 20/12/2018
* @description Test class for TriggerHandlerTemplate class
*/

@isTest
private class TriggerHandlerTemplateTest {

    private static String triggerObject = 'Contact';
    private static Boolean isBefore = false;
    private static Boolean isDelete = false;
    private static Boolean isAfter = false;
    private static Boolean isInsert = false;
    private static Boolean isUpdate = false;
    private static Boolean isUndelete = false;
    private static Boolean isExecuting = false;
    private static List<SObject> newlist = new List<SObject>();
    private static Map<Id, SObject> newmap = new Map<Id, SObject>();
    private static List<SObject> oldlist = new List<SObject>();
    private static Map<Id, SObject> oldmap = new Map<Id, SObject>();

    @isTest static void mainEntryBeforeTest(){

        try {
            TriggerHandlerTemplate tBase = new TriggerHandlerTemplate();
            isBefore = true;

            isInsert = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            isInsert = false;
            
            isUpdate = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            isUpdate = false;
            
            isDelete = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            System.assert(true, 'No exceptions catched');
        } catch(Exception e){
            System.assert(false, e.getMessage());
        }
    }

    @isTest static void mainEntryAfterTest(){

        try {
            TriggerHandlerTemplate tBase = new TriggerHandlerTemplate();
            isAfter = true;

            isInsert = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            isInsert = false;
            
            isUpdate = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            isUpdate = false;
            
            isDelete = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
            isDelete = false;

            isUndelete = true;
            tBase.mainEntry(triggerObject, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);

            System.assert(true, 'No exceptions catched');

        } catch(Exception e){
            System.assert(false, e.getMessage());
        }
    }

    @isTest static void inProgressEntryTest() {

        try {
            TriggerHandlerTemplate tBase = new TriggerHandlerTemplate();
            tBase.inProgressEntry(triggerObject+TriggerConstant.TRIGGER_HANDLER_STRING, 
                isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                newlist, newmap, oldlist, oldmap);
        } catch(Exception e){
            System.assert(false, e.getMessage()+e.getStackTraceString());
        }
    }
}