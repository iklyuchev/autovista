public class DeleteIntegrationLogSchedule implements Schedulable{

    public void execute(SchedulableContext sc) {
       Database.executeBatch(new DeleteIntegrationLogBatch(), 200);
    }
}