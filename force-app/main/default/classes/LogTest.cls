/**
* @author Sapienti7
* @date 31/12/2018
* @description Test class for Log apex class
*/
@IsTest
private class LogTest {

    public static Account testAccount;
    
    @TestSetup
    static void makeData(){
        TestDataFactory.insertCustomSettings(false, false, true);
        
        testAccount = new Account();
    }

    @isTest
    static void testLogPositive(){
        Test.startTest();
        Log.push('This is a log');
        Log.warn('This is a warning');
        Log.emit();
        Test.stopTest();
        List<LogRecording__c> logs = [Select Id, Severity__c FROM LogRecording__c];
//        System.assert(logs.size() > 0, 'Log size should be one');
    }

    @isTest static void testLogException(){
        try{
            Test.startTest();
            insert testAccount;
            Test.stopTest();
            System.assert(false, 'Account error should prevent this assertion from being called');
        }catch (Exception e){
            Log.push('This is a log');
            Log.error(e);
            Log.error('This is a message', e);
            Log.emit();
            System.assert(true, 'Account error should be caught');
        }
    }

    @isTest static void pushMethodTest(){
        Test.startTest();
        String testString = 'Test';
        Log.push(testString);
        Test.stopTest();
        System.assertEquals(testString, Log.current.scope, 'Expected scope to be set to \'' + testString + '\'');
    }

    @isTest 
    static void flushMethodTest(){
        Test.startTest();
        String testString = 'Test';
        String testMessage = 'TestMessage';
        Log.push(testString);
        Log.info(testMessage);
        String testLog = Log.flush();
        Test.stopTest();
        System.assert(testLog.contains(testString), 'Log expected to contain \'' + testString + '\'');
        System.assert(testLog.contains('INFO - ' + testMessage), 'Log expected to contain \'' + testMessage + '\'');
    }

    @isTest static void emitMethodTest(){
        Test.startTest();
        Log.push('Test');
        Log.info('TestMessage');
        Log.emit();
        Test.stopTest();
        List<EventBusSubscriber> eventBusSubscriber = [ SELECT Name, Position FROM EventBusSubscriber WHERE Topic = 'Log__e' ];
        System.assertEquals(1, eventBusSubscriber.get(0).Position, 'Platform event is expected to be in first position.');
       
   }

    @isTest static void popMethodTest(){
        Test.startTest();
        String testString1 = 'Test1';
        String testString2 = 'Test2';
        Log.push(testString1);
        
        System.assertEquals(testString1, Log.current.scope, 'Expected scope to be set to \'' + testString1 + '\'');
        
        Log.push(testString2);
    
        System.assertEquals(testString2, Log.current.scope, 'Expected scope to be set to \'' + testString2 + '\'');
        
        Log.pop();
       
        System.assertEquals(testString1, Log.current.scope, 'Expected scope to be set to \'' + testString1 + '\'');

        Test.stopTest();
    }

    @isTest static void logInfoTest(){
        Test.startTest();
        Log.push('Test');
        Log.info('TestMessage');
        
        Test.stopTest();
        System.assertEquals(1, Log.current.logEntries.size(), 'Expected one log entry');
        System.assert(Log.current.severity.contains('Info'), 'Expected severity to be set to info');
        
    }

    @isTest static void logDebugTest(){
        Test.startTest();
        Log.push('Test');
        Log.debug('TestMessage');
        Test.stopTest();
        System.assertEquals(1, Log.current.logEntries.size(), 'Expected one log entry');
        System.assert(Log.current.severity.contains('Debug'), 'Expected severity to be set to debug');
    }

    @isTest static void logWarningTest(){
        Test.startTest();
        Log.push('Test');
        Log.warn('TestMessage');
        Test.stopTest();
        System.assertEquals(1, Log.current.logEntries.size(), 'Expected one log entry');
        System.assert(Log.current.severity.contains('Warning'), 'Expected severity to be set to warning');
        
    }

    @isTest static void logErrorMessageTest(){
        Test.startTest();
        Log.push('Test');
        Log.error('TestMessage');
        
        Test.stopTest();
        System.assertEquals(1, Log.current.logEntries.size(), 'Expected one log entry');
        System.assert(Log.current.severity.contains('Error'), 'Expected severity to be set to error');
     }

    @isTest static void logErrorExceptionTest(){
        Test.startTest();
        Log.push('Test');
         try{
            insert testAccount;
        } catch(Exception testException){
            Log.error(testException);
        }
        Test.stopTest();
        System.assertEquals(2, Log.current.logEntries.size(), 'Expected two log entry');
        System.assert(Log.current.severity.contains('Error'), 'Expected severity to be set to error');
    }

    @isTest static void logErrorMessageAndExceptionTest(){
        Test.startTest();
        Log.push('Test');
        try{
            insert testAccount;
        } catch(Exception testException){
            Log.error('TestMessage', testException);
        }
        Test.stopTest();
        System.assertEquals(2, Log.current.logEntries.size(), 'Expected two log entry');
        System.assert(Log.current.severity.contains('Error'), 'Expected severity to be set to error');
    }
    
    @isTest static void logScopeAsString(){
        Test.startTest();
        Log.currentSeverity = 'Error';
        Log.root.logEntries = new List<Object>{'Error'};
        Log.emit();
        Test.stopTest();
        List<LogRecording__c> logs = [Select Id, Severity__c FROM LogRecording__c];
//        System.assert( logs.size() > 0, 'Expected one log record');
//        System.assertEquals('Error', logs[0].Severity__c, 'Expected severity to be set to error');
    }
}