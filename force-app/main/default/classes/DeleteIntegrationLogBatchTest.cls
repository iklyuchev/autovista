/**
* @author 		Sapienti7
* @date 		15/07/2019
* @description  Unit test for DeleteIntegrationLogBatch. Test clean up Integration log records which are older
*               than 3 months.
*/
@isTest
private class DeleteIntegrationLogBatchTest {
    
    @testSetup
    private static void setupTestData() {
        TestDataFactory.insertCustomSettings(false, false, true);
        
        Account account = TestDataFactory.createAccount(true, 'Test');

        Date dateBeforeThreeMonths = System.today().addMonths(-4);
        Date dateExactlyBeforeThreeMonths = System.today().addMonths(-3);
        Date dateBeforeOneMonth = System.today().addMonths(-1);

        Integration_Log__c log1 = TestDataFactory.createIntegrationLog(true, 'SUN', account.Id, 'New', 'Test 1');
        Test.setCreatedDate(log1.Id, dateBeforeThreeMonths);

        Integration_Log__c log2 = TestDataFactory.createIntegrationLog(true, 'SUN', account.Id, 'New', 'Test 2');
        Test.setCreatedDate(log2.Id, dateExactlyBeforeThreeMonths);

        Integration_Log__c log3 = TestDataFactory.createIntegrationLog(true, 'SUN', account.Id, 'New', 'Test 3');
        Test.setCreatedDate(log3.Id, dateBeforeOneMonth);
    }

    @isTest
	private static void testDeleteOlderLogs() {
		
        Test.startTest();

        Database.executeBatch(new DeleteIntegrationLogBatch());

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id,Target_Object__c FROM Integration_Log__c];
        System.assertEquals(1, logs.size(), '3 months or older logs must be deleted.');

        System.assertEquals('Test 3', logs[0].Target_Object__c, 'Only logs older than 3 months must be deleted.');
	}

    @isTest
	private static void testDeleteNewerLogs() {

        Integration_Log__c log = TestDataFactory.createIntegrationLog(true, 'SUN', null, 'New', 'Test');
        Test.setCreatedDate(log.Id, System.today());

		Test.startTest();

        Database.executeBatch(new DeleteIntegrationLogBatch());

        Test.stopTest();

        log = [SELECT Id,Target_Object__c FROM Integration_Log__c WHERE Id =: log.Id LIMIT 1];
        System.assertNotEquals(null, log, 'Log record created today must not be deleted.');
	}

    @isTest
	private static void testWithDMLException() {
        
        List<Integration_Log__c> logs = [SELECT Id FROM Integration_Log__c];
        delete logs;

		Test.startTest();

        DeleteIntegrationLogBatch batch = new DeleteIntegrationLogBatch();
        batch.deleteIntegrationLogs(logs);

        Test.stopTest();

        logs = [SELECT Id FROM Integration_Log__c WHERE IsDeleted = true ALL ROWS];
        System.assertEquals(3, logs.size(), 'Integration Logs must not be deleted from recycle bin.');
	}
}