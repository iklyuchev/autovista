/**
* @author Sapienti7
* @date 20/12/2018
* @description Test class for TriggerDispatcher class
*/

@isTest
private class TriggerDispatcherTest {

    private static String triggerObject = 'Contact';
    private static Boolean isBefore = false;
    private static Boolean isDelete = false;
    private static Boolean isAfter = false;
    private static Boolean isInsert = false;
    private static Boolean isUpdate = false;
    private static Boolean isUndelete = false;
    private static Boolean isExecuting = false;
    private static List<SObject> newlist = new List<SObject>();
    private static Map<Id, SObject> newmap = new Map<Id, SObject>();
    private static List<SObject> oldlist = new List<SObject>();
    private static Map<Id, SObject> oldmap = new Map<Id, SObject>();

    @isTest static void mainEntryTest() {

        Test.startTest();
        try {

            TriggerDispatcher.mainEntry(triggerObject, 
                    isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);
            
            System.assert(true, 'No exceptions catched');
        } catch(Exception e){
            System.assert(false, e.getStackTraceString());
        }
        Test.stopTest();
    }

    @isTest static void mainEntryNullPointerExceptionTest() {

        Test.startTest();
        try {
            triggerObject = 'NonExistingClass';
            TriggerDispatcher.mainEntry(triggerObject, 
                    isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);
            System.assert(false, 'Class with name NonExistingClassTriggerHandler actually exists!');
        } catch(Exception e){
            System.assert(true, e.getStackTraceString());
        }
        Test.stopTest();
    }

    @isTest static void triggersOffTest() {
        Bypass_Configuration__c bc = new Bypass_Configuration__c(IsTriggerFrameworkBypassed__c = true);
        insert bc;

        Test.startTest();
        try {

            TriggerDispatcher.mainEntry(triggerObject, 
                    isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);
            
            System.assert(true, 'No exceptions catched');
        } catch(Exception e){
            System.assert(false, e.getStackTraceString());
        }
        Test.stopTest();
    }

    @isTest static void inprogressEntryTest() {

        Test.startTest();
        try {

            TriggerDispatcher.mainEntry(triggerObject, 
                    isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);

            TriggerDispatcher.mainEntry(triggerObject, 
                    isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);
            
            System.assert(true, 'No exceptions catched');
        } catch(Exception e){
            System.assert(e != null);
        }
        Test.stopTest();
    }
}