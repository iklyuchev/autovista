/**
* @author 		Sapienti7
* @date 		10/05/2019
* @description  Unit test for IntegrationHelper. Tests Account and Transaction record data
*               synchronizing with SUN.
*/
@isTest
private class IntegrationHelperTest {
    
    @testSetup
	private static void setupData() {

        TestDataFactory.insertCustomSettings(true, false, true);

		List<Account> accounts = TestDataFactory.createAccounts(true, 2);
        Set<Id> accountIds = new Set<Id>{accounts[1].Id};

        TestDataFactory.createIntegrationLogs(
            true, 
            IntegrationHelper.INTEG_LOG_INTEGRATION, 
            accountIds, 
            IntegrationHelper.INTEG_LOG_NEW_STATUS, 
            IntegrationHelper.INTEG_LOG_TARGET_OBJECT);

        TestDataFactory.createAssets(true, 1, accounts[0].Id, null, System.today(), 12, false);
	}

    @isTest
    private static void testSetupAccountsCurrentlyInSync() {
        
        Test.startTest();

        IntegrationHelper.setupAccountsCurrentlyInSync();

        Test.stopTest();

        System.assertEquals(1, IntegrationHelper.accountIdsCurrentlyInSync.size(), 'Invalid number of Logs Currently in Sync.');
    }

    @isTest
    private static void testSetupAccountIdsToUpdate() {
        
        IntegrationHelper.accountSyncFields = new Set<String>{'Name'};

        Account oldAccount = [SELECT Id, Name, Bypass_Bulk_Upload__c FROM Account LIMIT 1];
        Account newAccount = oldAccount.clone(true, true, true, true);
        newAccount.Name = 'TEST CHANGED ACCOUNT NAME';

        Test.startTest();

        IntegrationHelper.setupAccountIdsToUpdate(newAccount, oldAccount);

        Test.stopTest();

        System.assert(IntegrationHelper.accountIdsToSync.contains(newAccount.Id), newAccount.Name+' Account should be added for sync list.');
    }

    @isTest
    private static void testIsAvailableForUpdatePositive() {
        Account account = [SELECT Id, Name, Account_Status__c, Bypass_Bulk_Upload__c FROM Account LIMIT 1];
        
        Test.startTest();

        Boolean isAvailable = IntegrationHelper.isAvailableForUpdate(account);

        Test.stopTest();

        System.assert(isAvailable, 'Account should be available for sync.');
    }

    @isTest
    private static void testIsAvailableForUpdateBulkFlagSet() {
        Account account = [SELECT Id, Name, Account_Status__c, Bypass_Bulk_Upload__c FROM Account LIMIT 1];
        
        Test.startTest();

        account.Bypass_Bulk_Upload__c = true;
        Boolean isAvailable = IntegrationHelper.isAvailableForUpdate(account);

        Test.stopTest();

        System.assert(!isAvailable, 'Account should not be available for sync.');
    }

    @isTest
    private static void testIsAvailableForUpdateAlreadyInSync() {
        Account account = [SELECT Id, Name, Account_Status__c, Bypass_Bulk_Upload__c FROM Account LIMIT 1];
        
        Test.startTest();

        account.Bypass_Bulk_Upload__c = false;
        IntegrationHelper.accountIdsCurrentlyInSync.add(account.Id);
        Boolean isAvailable = IntegrationHelper.isAvailableForUpdate(account);

        Test.stopTest();

        System.assert(!isAvailable, 'Account should not be available for sync.');
    }

    @isTest
    private static void testFieldSetFields() {

        Test.startTest();

        Set<String> fields = IntegrationHelper.getAccountSyncFields();

        Test.stopTest();

        List<Schema.FieldSetMember> fieldSetfields = SObjectType.Account.FieldSets.SUN_Account_Sync_Fields.getFields();
        System.assertEquals(fieldSetfields.size(), fields.size(), 'Fieldset fields number does not match with retrieved fields.');
    }

    @isTest
    private static void testFilterAndCreateIntegrationLogs() {

        Asset a = [SELECT Id, AccountId from Asset LIMIT 1];

        Test.startTest();

        IntegrationHelper.accountIdsToSync.add(a.AccountId);
        IntegrationHelper.filterAndCreateIntegrationLogs();

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, Integration__c, Status__c, Target_Object__c, Target_Id__c FROM Integration_Log__c WHERE Target_Id__c = :a.AccountId];
        System.assertEquals(1, logs.size(), 'Integration logs are not created.');
        System.assertEquals(IntegrationHelper.INTEG_LOG_INTEGRATION, logs[0].Integration__c, 'Invalid Integration is set on log '+ logs[0].Integration__c);
        System.assertEquals(IntegrationHelper.INTEG_LOG_NEW_STATUS, logs[0].Status__c, 'Invalid Status is set on log '+ logs[0].Status__c);
        System.assertEquals(IntegrationHelper.INTEG_LOG_TARGET_OBJECT, logs[0].Target_Object__c, 'Invalid Target Object is set on log '+ logs[0].Target_Object__c);
        
    }

    @isTest
    private static void testFilterAndCreateIntegrationLogsException() {

        Asset a = [SELECT Id, AccountId from Asset LIMIT 1];

        Test.startTest();

        IntegrationHelper.simulateDMLException = true;
        IntegrationHelper.accountIdsToSync.add(a.AccountId);
        IntegrationHelper.filterAndCreateIntegrationLogs();

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, Integration__c, Status__c, Target_Object__c, Target_Id__c FROM Integration_Log__c WHERE Target_Id__c = :a.AccountId];
        System.assertEquals(0, logs.size(), 'Integration logs are not created.');
    }
}