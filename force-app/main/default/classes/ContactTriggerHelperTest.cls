/**
* @author Sapienti7
* @date 28/04/2019
* @description Unit test for Contract Trigger Helper.
*/
@IsTest
private class ContactTriggerHelperTest {

    @TestSetup
    private static void setupData() {
        TestDataFactory.insertCustomSettings(false, false, true);

        Account account = TestDataFactory.createAccount(true, 'Test');

        TestDataFactory.createContacts(true, 200, account.Id);
    }

    /**
    * @description Ensure an error is thrown when trying to delete a primary contact.
    */
    @isTest
    private static void testDeletePrimaryContact() {
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 200];

        contacts[1].Primary_Contact__c = true;
        update contacts[1];
        try {
            Test.startTest();

            delete contacts[1];

            Test.stopTest();

            System.assert(false, 'Exception is expected on deleting primary contact');
        } catch (Exception ex) {
            System.assertEquals(true, ex.getMessage().containsIgnoreCase(Label.PrimaryContactDeleteError), 'Invalid validation error message.');
        }
        List<Contact> contactsDeleteOp = [SELECT Id, Name FROM Contact WHERE Id=:contacts[1].Id LIMIT 1];

        System.assertEquals(1, contactsDeleteOp.size(), 'Contact '+contactsDeleteOp[0].Name+' should not be deleted.');
    }

    /**
    * @description Ensure error message is thrown when trying to delete a list of contacts including a primary contact. 
    */
    @isTest
    private static void testDeletePrimaryContactBulk() {
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 200];

        contacts[1].Primary_Contact__c = true;
        update contacts[1];

        try {
            Test.startTest();

            delete contacts;

            Test.stopTest();

            System.assert(false, 'Exception is expected on deleting primary contact');
        } catch (Exception ex) {
            System.assertEquals(true, ex.getMessage().containsIgnoreCase(Label.PrimaryContactDeleteError), 'Invalid validation error message.');
        }
         contacts = [SELECT Id FROM Contact LIMIT 200];

         System.assertEquals(200, contacts.size(), 'Contacts should not be deleted.');
    }

    /**
    * @description Ensure contacts can be deleted when they are not marked as primary contacts.
    */
    @isTest
    private static void testDeleteStandardContact() {
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 200];

        Test.startTest();

        delete contacts;

        Test.stopTest();

        contacts = [SELECT Id FROM Contact LIMIT 200];
        System.assertEquals(0, contacts.size(), 'All Contacts should be deleted.');
    }
}