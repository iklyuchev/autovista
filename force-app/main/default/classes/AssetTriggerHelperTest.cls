/**
* @author       Sapienti7
* @date         11/07/2019
* @description  Unit test for AssetTriggerHandler. Tests creation of Integration logs on inserting Assets.
*/
@isTest
private class AssetTriggerHelperTest {

    @testSetup
    private static void setupData() {
        TestDataFactory.insertCustomSettings(false, false, true);
        
        Account account = TestDataFactory.createAccount(true, 'Test');
        List<Opportunity> opportunities = TestDataFactory.createOpportunities(true, 5, account.Id);

        List<Contact> contacts = TestDataFactory.createContacts(true, 1, account.Id);
        TestDataFactory.createOpportunityContactRoles(true, opportunities, contacts);

        List<Product2> products = TestDataFactory.createProductsWithPriceBook(1, 25.00, false);
        TestDataFactory.createOpportunityLineItems(true, opportunities, products, 30.00, 1);
    }

    @isTest
    private static void testOnActiveAssetCreation() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        List<Asset> activeAssets = TestDataFactory.createAssets(false, 10, account.Id, product.Id, System.today()-1, 12, false);
    
        Test.startTest();

        insert activeAssets;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, 
                                            Integration__c, 
                                            Status__c, 
                                            Target_Object__c, 
                                            Target_Id__c FROM Integration_Log__c 
                                            WHERE Target_Id__c = :account.Id];
        System.assertEquals(1, logs.size(), 'Integration logs are not created for all the updated accounts');
        
        for(Integration_Log__c log : logs) {
            System.assertEquals(IntegrationHelper.INTEG_LOG_INTEGRATION, log.Integration__c, 'Invalid Integration is set on log '+ log.Integration__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_NEW_STATUS, log.Status__c, 'Invalid Status is set on log '+ log.Status__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_TARGET_OBJECT, log.Target_Object__c, 'Invalid Target Object is set on log '+ log.Target_Object__c);
        }
    }

    @isTest
    private static void testOnInactiveAssetCreation() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];

        List<Asset> inactiveAssets = TestDataFactory.createAssets(false, 10, account.Id, product.Id, System.today().addMonths(-12), 3, false);
    
        Test.startTest();

        insert inactiveAssets;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id FROM Integration_Log__c WHERE Target_Id__c = :account.Id];
        System.assertEquals(0, logs.size(), 'Integration logs should not be created for inactive assets');
    }

    @isTest
    private static void testOnInactiveAssetUpdate() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];

        List<Asset> pendingAssets = TestDataFactory.createAssets(true, 10, account.Id, product.Id, System.today().addMonths(1), 3, false);
    
        List<Integration_Log__c> logs = [SELECT Id FROM Integration_Log__c WHERE Target_Id__c = :account.Id];
        System.assertEquals(0, logs.size(), 'No Logs should be created for Pending Assets');

        Test.startTest();
        
        pendingAssets = [SELECT Id FROM Asset WHERE Id = :pendingAssets];
        for(Asset a : pendingAssets) {
            a.Current_Start_Date__c = System.today().addDays(-2);
        }
        update pendingAssets;

        Test.stopTest();

        logs = [SELECT Id FROM Integration_Log__c WHERE Target_Id__c = :account.Id];
//        System.assertEquals(1, logs.size(), 'Integration log should be created for activated assets');
    }

    @isTest
    private static void testWithExistingIntegrationLogs() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];

        List<Asset> activeAssets = TestDataFactory.createAssets(false, 10, account.Id, product.Id, System.today()-1, 12, false);
    
        TestDataFactory.createIntegrationLog(
            true, 
            IntegrationHelper.INTEG_LOG_INTEGRATION, 
            account.Id, 
            IntegrationHelper.INTEG_LOG_NEW_STATUS, 
            IntegrationHelper.INTEG_LOG_TARGET_OBJECT);

        Integration_Log__c log = [SELECT Id FROM Integration_Log__c LIMIT 1];

        Test.startTest();

        insert activeAssets;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id FROM Integration_Log__c WHERE Id !=: log.Id];

        System.assertEquals(0, logs.size(), 'Additional log record should not be created for existing Account.');
    }

    @isTest
    private static void testWithClosingMultipleOpportunities() {
        
        List<Opportunity> opps = [SELECT Id FROM Opportunity];

        Test.startTest();

        for(Opportunity opp : opps) {
            opp = TestUtils.closeWinOpportunity(opp);
        }

        update opps;

        Test.stopTest();

        List<Integration_Log__c> logs = [SELECT Id, 
                                            Integration__c, 
                                            Status__c, 
                                            Target_Object__c, 
                                            Target_Id__c FROM Integration_Log__c];
        System.assertEquals(1, logs.size(), 'Integration logs are not created for all the updated accounts');
        
        for(Integration_Log__c log : logs) {
            System.assertEquals(IntegrationHelper.INTEG_LOG_INTEGRATION, log.Integration__c, 'Invalid Integration is set on log '+ log.Integration__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_NEW_STATUS, log.Status__c, 'Invalid Status is set on log '+ log.Status__c);
            System.assertEquals(IntegrationHelper.INTEG_LOG_TARGET_OBJECT, log.Target_Object__c, 'Invalid Target Object is set on log '+ log.Target_Object__c);
        }
    }
}