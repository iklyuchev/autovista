/**
* @author       Sapienti7
* @date         21/06/2019
* @description  Provides functionality to query trialable products for a give opportunity.
*               Provides functionalty to create Trial Opportunities, Assets and Opportunity Assets.
*/
public with sharing class TrialHelper {
    
    public static Id trialRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Trial').getRecordTypeId();
    public static final String TRIAL = System.Label.Trial;
    public static final String OFFERED_STAGE = Label.OpportunityOfferedStage;

    /**
    * @description  Provides related Opportunity, Opportunity Line Item and Asset information.
    * @param        Id opportunityId - Opportunity record Id
    * @return       String - JSON serialised string of list of trialble products and already trialed products.
    */
    @AuraEnabled(cacheable=true)
    public static String getOpportunityTrialData(Id opportunityId) {

        if(opportunityId != null) {

            Set<Id> currentlyTrialed = getCurrentlyTrialedProducts(opportunityId);

            List<Opportunity> opportunity = [SELECT Id, AccountId, StageName,
                    (SELECT Id, TotalPrice, Product2Id, Product2.Id, Product2.Name, Product2.Is_Free_Trial__c 
                        FROM OpportunityLineItems 
                        WHERE  Product2Id != :currentlyTrialed AND Product2.Is_Free_Trial__c = true
                        ORDER BY Product2.Name) 
                    FROM Opportunity 
                    WHERE Id =: opportunityId LIMIT 1];

            return JSON.serialize(opportunity[0]);
        }
        return null;
    }

   /**
    * @description  Creates Trial Opportunity, Assets and Opportunity Asset records for a given list of Products.
    * @param        Id opportunityId - Opportunity record Id
    * @param        String products - JSON serialised list of products to be trialed.
    * @return       void
    */
    @AuraEnabled
    public static void createOpportunityTrials(Id opportunityId, String selectedTrials) {
        
        if (Schema.sObjectType.Opportunity.isCreateable()) {
            Savepoint beforeTransactions = Database.setSavepoint();
            try {
                Map<String, Object> trialData = (Map<String, Object>)JSON.deserializeUntyped(selectedTrials);
                Integer duration = Integer.valueOf(trialData.get('duration'));
                List<Object> items = (List<Object>) trialData.get('items');

                Set<Id> itemIds = new Set<Id>();
                for(Object prod : items) {
                   itemIds.add((Id)prod);
                }
                Opportunity parentOpportunity = [SELECT Id, Pricebook2Id, AccountId, CurrencyIsoCode, Company__c, Sales_Country__c, Country__c,
                    (SELECT Id, Pricebook2Id, AccountId, CurrencyIsoCode FROM Opportunities__r WHERE RecordTypeId = :trialRecordTypeId AND IsClosed = false LIMIT 1),
                    (SELECT Id, Product2Id, PricebookEntryId, Discount, Quantity, UnitPrice, Description, Revenue_Type__c 
                        FROM OpportunityLineItems WHERE Id = :itemIds),
                    (SELECT ContactId, OpportunityId, IsPrimary, Role FROM OpportunityContactRoles) 
                    FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
                Opportunity trialOpp;
                if(parentOpportunity.Opportunities__r.size() > 0) {
                    trialOpp = upsertTrialOpportunity(parentOpportunity.Opportunities__r[0], duration);
                } else {
                    trialOpp = createNewTrialOpportunity(parentOpportunity);

                    trialOpp.Pricebook2Id = trialOpp.Pricebook2Id == null ? parentOpportunity.Pricebook2Id : trialOpp.Pricebook2Id;
                    upsertTrialOpportunity(trialOpp, duration);
                }
                createOppLineItems(parentOpportunity.OpportunityLineItems, trialOpp);
                
            } catch (Exception ex) {
                Database.rollback(beforeTransactions);
                throw new  AuraHandledException(ex.getMessage()+':'+ex.getStackTraceString());
            }
        } else {
            throw new AuraHandledException(System.Label.InsufficientAccess);
        }
    }

    /**
    * @description  Creates a new Trial Opportunity Record with Parent Opportunity details.
    * @param        Opportunity parentOpportunity - Parent Opportunity which Trial will be created against.
    * @param        Duration of the Trial in Days
    * @return       Trial Opportunity record.
    */
    public static Opportunity createNewTrialOpportunity(Opportunity parentOpportunity) {
        //Create trial Opportunity
        Opportunity trialOpp = new Opportunity(
            Name = TRIAL,
            Type = TRIAL,
            Product_Summary__c = '-',
            AccountId = parentOpportunity.AccountId,
            CloseDate = System.today(), 
            Start_Date__c =  System.today(),
            Parent_Opportunity__c = parentOpportunity.Id,
            Pricebook2Id = parentOpportunity.Pricebook2Id,
            CurrencyISOCode = parentOpportunity.CurrencyISOCode,
            Company__c = parentOpportunity.Company__c,
            Sales_Country__c = parentOpportunity.Sales_Country__c,
            Country__c = parentOpportunity.Country__c,
            StageName = OFFERED_STAGE,
            RecordTypeId = trialRecordTypeId);

        return trialOpp;
    }

     /**
    * @description  For a given opportunity id, retrieve all product ids which are currently trialled and all
    *               free trials.
    * @param        Id opportunityId - Opportunity record Id
    * @return       Set<Id> currently trialled product ids.
    */
    @Testvisible
    private static Set<Id> getCurrentlyTrialedProducts(Id opportunityId) {
        //Get product ids from Trial opp
        List<OpportunityLineItem> trialItems = [SELECT Product2Id
                    FROM OpportunityLineItem 
                    WHERE Opportunity.Parent_Opportunity__c =: opportunityId 
                        AND Opportunity.RecordType.DeveloperName = 'Trial' AND Opportunity.IsClosed = false];
        
        Set<Id> currentlyTrialedProducts = new Set<Id>();

        for(OpportunityLineItem trialItem : trialItems) {
            currentlyTrialedProducts.add(trialItem.Product2Id);
        }
        //Get product ids from Assets
        Opportunity opp = [SELECT AccountId FROM Opportunity WHERE Id =: opportunityId LIMIT 1];
        if(opp.AccountId != null) {
            List<Asset> assets = [SELECT Product2Id FROM Asset WHERE AccountId =: opp.AccountId AND Is_Free_Trial__c = true];
            for(Asset a : assets) {
                currentlyTrialedProducts.add(a.Product2Id);
            }
        }
        return currentlyTrialedProducts;
    }

    

    /**
    * @description  Insert or update Trial Opportunity Record with Start Date, End Date and Trial Duration.
    * @param        Current Trial record
    * @param        Duration of the Trial in Days
    * @return       Trial Opportunity record.
    */
    @Testvisible
    private static Opportunity upsertTrialOpportunity(Opportunity currentTrialOpportunity, Integer duration) {
        
        currentTrialOpportunity.StageName = Label.OpportunityOfferedStage;
        currentTrialOpportunity.CloseDate = System.today();
        currentTrialOpportunity.Start_Date__c =  System.today();
        currentTrialOpportunity.Trial_Duration__c = duration;

        upsert currentTrialOpportunity;
        return currentTrialOpportunity;
    }

    /**
    * @description  Creates Opportunity Line Items for Trial Opportunity.
    * @param        List<OpportunityLineItem> selectedParentLineItems - Selected Trial Opportunity Line Items from the Parent Opportunity.
    * @param        Opportunity trialOpp - Trial Opportunity record.
    */
    @TestVisible
    private static void createOppLineItems(List<OpportunityLineItem> selectedParentLineItems, Opportunity trialOpp) {
        //create Opportunity Line Items
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

        for(OpportunityLineItem oppLine : selectedParentLineItems) {
            OpportunityLineItem newOppLine = createOpportunityLineItem(null, trialOpp.Id, oppLine.PricebookEntryId, oppLine.Revenue_Type__c, oppLine.Quantity, 0.0, true);
            oppLineItems.add(newOppLine);
            
        }
        insert oppLineItems;
    }

    public static OpportunityLineItem createOpportunityLineItem(Id productId, Id opportunityId, Id pricebookEntryId, String revenueType, Decimal quantitiy, Decimal unitPrice, Boolean isFreeTrial) {
        OpportunityLineItem trialOppLine = new OpportunityLineItem(
                        Product2Id = productId,
                        OpportunityId = opportunityId,
                        Revenue_Type__c = revenueType,
                        Quantity = quantitiy,
                        UnitPrice = unitPrice,
                        Is_Free_Trial__c = isFreeTrial);
        if(pricebookEntryId <> null) trialOppLine.PricebookEntryId = pricebookEntryId;
                    
        return trialOppLine;
    }
    
    public static void createTrialContactRoles(Map<Id, Opportunity> trialOpportunitiesMap) 
    {
        List<OpportunityContactRole> newContactRoles = new List<OpportunityContactRole>();
        
        List<OpportunityContactRole> oppContRoles = [SELECT OpportunityId, ContactId, IsPrimary, Role FROM OpportunityContactRole 
            WHERE OpportunityId IN :trialOpportunitiesMap.keySet()];
        for(OpportunityContactRole role: oppContRoles)
        {
            OpportunityContactRole newContactRole = role.clone(false);
            newContactRole.OpportunityId = trialOpportunitiesMap.get(role.OpportunityId).Id;
            newContactRoles.add(newContactRole);
        }
        
        List<Database.SaveResult> saveResults = Database.insert(newContactRoles, false);
        Utils.logSaveResults(saveResults);
    }
}