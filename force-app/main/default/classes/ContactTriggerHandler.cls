/**
* @author Sapienti7
* @date 28/04/2019
* @description TriggerHandler for Contact object.
*/
public with sharing class ContactTriggerHandler extends TriggerHandlerTemplate {

    /**
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on before delete trigger event
	*/
    public override void onBeforeDelete(List<SObject> oldlist, Map<Id, SObject> oldmap){
        //Add error on deletion of Primary Contacts
        ContactTriggerHelper.validatePrimaryContact(oldlist);
    }
}