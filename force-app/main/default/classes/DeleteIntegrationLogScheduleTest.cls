/**
* @author 		Sapienti7
* @date 		15/07/2019
* @description  Unit test for  DeleteIntegrationLogSchedule. Tests DeleteIntegrationLogBatch can be scheduled.
*/
@isTest
private class DeleteIntegrationLogScheduleTest {
    
   @isTest
	private static void testScheduelable() {

        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
		Test.startTest();

        String jobId = System.schedule('testDeleteIntegLogSchedule', CRON_EXP, new DeleteIntegrationLogSchedule());

        Test.stopTest();

        System.assertNotEquals(null, jobId, 'Scheduled job must be created.');
        CronTrigger ct = [SELECT Id, CronExpression FROM CronTrigger WHERE Id = :jobId];

        System.assertEquals(CRON_EXP, ct.CronExpression, 'Invalid CRON Expression');
	}
}