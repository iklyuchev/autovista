/**
* @author Sapienti7
* @date 11/07/2019
* @description TriggerHandler for Account object.
*/
public with sharing class AccountTriggerHandler extends TriggerHandlerTemplate {

    public override void onAfterUpdate(List<SObject> newList, Map<Id, SObject> newMap, List<SObject> oldList, Map<Id, SObject> oldMap){
        
        IntegrationHelper.setupAccountsCurrentlyInSync();
        
        for(Account newAccount : (List<Account>) newlist) {
            Account oldAccount = (Account) oldMap.get(newAccount.Id);

            IntegrationHelper.setupAccountIdsToUpdate(newAccount, oldAccount);
        }

        IntegrationHelper.filterAndCreateIntegrationLogs();
    }
}