public class Utils {

    private static Map<String,Map<String,Schema.SObjectField>> fieldsMap;

    public static void logSaveResults(List<Database.SaveResult> saveResults) {
        for(Database.SaveResult saveResult : saveResults) {
                if(!saveResult.isSuccess()) {
                    Log.push('Utils.logSaveResults');
                    for(Database.Error err : saveResult.getErrors()) {
                        Log.error(err.message+':'+err.getStatusCode()+':'+err.getFields());
                        Log.pop();
                        Log.emit();
                    }
                }
            }
    }

    public static Set<String> getAllFields(String objectName) {
        if(objectName != null) {
            if(fieldsMap != null && fieldsMap.containsKey(objectName)) {
                return fieldsMap.get(objectName).keySet();
            } else {
                SObjectType objType = Schema.getGlobalDescribe().get(objectName);
                if(objType != null) {
                    fieldsMap = new Map<String,Map<String,Schema.SObjectField>>{
                        objectName => objType.getDescribe().fields.getMap()
                    };
                    return fieldsMap.get(objectName).keySet();
                }
            }
        }
        return null;
    }

    public static Boolean runValidation() {
        Bypass_Configuration__c config = Bypass_Configuration__c.getInstance();
        return config == null ? true : !config.IsValidationBypassed__c;
    }
}