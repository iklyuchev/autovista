/**
* @author Sapienti7
* @date 11/07/2019
* @description Helper to implement SUN Integration related functionality.
*/
public class IntegrationHelper {
    public static final String INTEG_LOG_INTEGRATION = System.Label.IntegrationLogSUNIntegration;
    public static final String INTEG_LOG_NEW_STATUS = System.Label.IntegrationLogNewStatus;
    public static final String INTEG_LOG_EXTRACTED_STATUS = System.Label.IntegrationLogExtractedStatus;
    public static final String INTEG_LOG_TARGET_OBJECT = System.Label.IntegrationLogAccountObject;

    public static Set<Id> accountIdsToSync = new Set<Id>();
    public static Set<Id> accountIdsCurrentlyInSync = new Set<Id>();

    @TestVisible
    private static Set<String> accountSyncFields = getAccountSyncFields();

    @TestVisible
    private static Boolean simulateDMLException = false;

    public static void setupAccountsCurrentlyInSync() {
        if(accountIdsCurrentlyInSync.size() == 0) {
            List<Integration_Log__c> logs = [SELECT Id, Target_Id__c FROM Integration_Log__c 
                WHERE Target_Object__c = :INTEG_LOG_TARGET_OBJECT 
                    AND Integration__c = :INTEG_LOG_INTEGRATION 
                    AND (Status__c = :INTEG_LOG_NEW_STATUS OR Status__c = :INTEG_LOG_EXTRACTED_STATUS)
                    AND Target_Id__c NOT IN: accountIdsToSync];

            for(Integration_Log__c log : logs) {
                accountIdsCurrentlyInSync.add(log.Target_Id__c);
            }
        }
   }

   public static void setupAccountIdsToUpdate(Account newAccount, Account oldAccount) {
       
        if(isAvailableForUpdate(newAccount)) {
            for(String field : accountSyncFields) {
                if(newAccount.get(field) != oldAccount.get(field)) {
                    accountIdsToSync.add(newAccount.Id);
                }
            }
        }
   }

   public static void filterAndCreateIntegrationLogs() {

        Log.push('IntegrationHelper.createIntegrationLogRecords');
        if(accountIdsToSync.size() > 0) {

            //Filter out any accounts do not have at least Active or Pending Asset.
            //This is done in the trigger as we do not calculate number of Active/Pending Assets on account.
            List<Account> accountsToSync = [SELECT Id, 
                (SELECT Id FROM Assets WHERE (Status = 'Active' OR Status = 'Pending') 
                    AND (Is_Free_Trial__c = false) LIMIT 1)
                FROM Account WHERE Id =: accountIdsToSync];

            List<Integration_Log__c> integrationLogs = new List<Integration_Log__c>();

            for(Account accountToSync : accountsToSync) {
                if(accountToSync.Assets.size() > 0) {
                    Integration_Log__c il = new Integration_Log__c();
                    il.Integration__c = INTEG_LOG_INTEGRATION;
                    il.Status__c = INTEG_LOG_NEW_STATUS;
                    il.Target_Object__c = INTEG_LOG_TARGET_OBJECT;
                    il.Target_Id__c = accountToSync.Id;
                    integrationLogs.add(il);
                }
            }
        
            try {
                if(simulateDMLException) {
                    throw new DmlException();
                }
                insert integrationLogs;
                //empty target account ids to be synced.
                accountIdsToSync.clear();
            } catch (Exception ex) {
                Log.error(String.valueOf(ex.getMessage()+'-'+ex.getStackTraceString()));
                Log.pop();
                Log.emit();
            }
        }
    }

    @TestVisible
    private static Boolean isAvailableForUpdate(Account newAccount) {
       return  !newAccount.Bypass_Bulk_Upload__c
                && !accountIdsCurrentlyInSync.contains(newAccount.Id);
   }

    @TestVisible
    private static Set<String> getAccountSyncFields() {
        if(accountSyncFields == null) {
            accountSyncFields = new Set<String>();

            List<Schema.FieldSetMember> fields = SObjectType.Account.FieldSets.SUN_Account_Sync_Fields.getFields();
        
            for(Schema.FieldSetMember fdmember : fields) {
                accountSyncFields.add(fdmember.getFieldPath());
            }
        }
        return accountSyncFields;
    }
}