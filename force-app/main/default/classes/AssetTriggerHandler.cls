/**
* @author Sapienti7
* @date 11/07/2019
* @description TriggerHandler for Asset object.
*/
public with sharing class AssetTriggerHandler extends TriggerHandlerTemplate {

    public static final String ASSET_ACTIVE_STATUS = 'Active';
    public static final String ASSET_PENDING_STATUS = 'Pending';
     /**
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after insert trigger event
	*/
    public override void onAfterInsert(List<SObject> newlist, Map<Id, SObject> newmap){

        IntegrationHelper.setupAccountsCurrentlyInSync();
        for(Asset a : (List<Asset>) newlist) {
            //If asset is not expired sync the related account
            Boolean isAvailableForSync = a.Asset_Status_On_Create__c == ASSET_ACTIVE_STATUS
                                            && (!IntegrationHelper.accountIdsCurrentlyInSync.contains(a.AccountId));
            
            if(isAvailableForSync) {
                IntegrationHelper.accountIdsToSync.add(a.AccountId);
            }
        }
        createIntegrationLogRecords();
    }

    /**
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after update trigger event
	*/
    public override void onAfterUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap){

        IntegrationHelper.setupAccountsCurrentlyInSync();
        for(Asset a : (List<Asset>) newlist) {
            Asset oldAsset = (Asset)oldmap.get(a.Id);
            //If asset is activated, create integration log.
            Boolean isAvailableForSync = a.Status == ASSET_ACTIVE_STATUS 
                                            && oldAsset.Status != ASSET_ACTIVE_STATUS
                                            && (!IntegrationHelper.accountIdsCurrentlyInSync.contains(a.AccountId));
            
            if(isAvailableForSync) {
                IntegrationHelper.accountIdsToSync.add(a.AccountId);
            }
        }
        createIntegrationLogRecords();
    }

    private static void createIntegrationLogRecords() {
        if(IntegrationHelper.accountIdsToSync.size() > 0) {
            IntegrationHelper.filterAndCreateIntegrationLogs();
        }
    }
}