/**
* @author       Sapienti7 te
* @date         10/05/2019
* @description  Unit test for OpportunityTriggerHelper. Tests regeneration of Opportunities and related records on
                Order won opportunity records.
*/
@isTest
private class OpportunityTriggerHelperTest {
    
    private static final string OPPORTUNITY_STAGE_WON = System.Label.OpportunityWonStage;
    private static final string OPPORTUNITY_STAGE_LOST = System.Label.OpportunityLostStage;
    private static final string OPPORTUNITY_CLONING_PROCESS = System.Label.OpportunityCloningProcess;
    private static final string OPPORTUNITY_RENEWAL_TYPE = System.Label.OpportunitySalesTypeRenewal;
    private static final string OPPORTUNITY_FIRST_CONTACT_STAGE = System.Label.OpportunityFirstContactStage;

    private static final integer BULK_RECORD_AMOUNT = 5;
    private static final integer GRACE_PERIOD = 5;

    @testSetup
    private static void setupData() {
        TestDataFactory.insertCustomSettings(false, false, true);
        Account account = TestDataFactory.createAccount(true, 'Test');
        List<Opportunity> opportunities = TestDataFactory.createOpportunities(true, BULK_RECORD_AMOUNT, account.Id);

        List<Contact> contacts = TestDataFactory.createContacts(true, 2, account.Id);
        TestDataFactory.createOpportunityContactRoles(true, opportunities, contacts);

        List<Product2> products = TestDataFactory.createProductsWithPriceBook(2, 25.00, false);
        TestDataFactory.createOpportunityLineItems(true, opportunities, products, 30.00, 2);

        List<Asset> assets = TestDataFactory.createAssets(true, BULK_RECORD_AMOUNT, account.Id, products[0].Id, System.today(), 12, false, GRACE_PERIOD);
        TestDataFactory.createOpportunityAssets(true, opportunities, assets);
    }

    @isTest
    private static void testOnUpdateCloningOpportunities(){
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];

        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        Test.startTest();

        update opp;

        Test.stopTest();

        String oppObjName = Schema.SObjectType.Opportunity.getName();
        
        List<Cloned_Object_Fields_Default_Value__mdt> defaultOppMeta = SObjectCloner.queryCustomMetadataTypes(OPPORTUNITY_CLONING_PROCESS, oppObjName);
        Set<String> fieldNames = new Set<String>();
        for(Cloned_Object_Fields_Default_Value__mdt mtd : defaultOppMeta) {
            fieldNames.add(mtd.Cloned_Field__r.QualifiedApiName);
        }

        Opportunity clonedOpp = Database.query('SELECT '+String.join(new List<String>(fieldNames), ',')+',Parent_Opportunity__c FROM Opportunity WHERE Parent_Opportunity__c IN: opps LIMIT 1');
        System.assertNotEquals(null, clonedOpp, 'Opportunity record must be cloned.');
        System.assertEquals(opps[0].Id, clonedOpp.Parent_Opportunity__c, 'Invalid Parent Opportunity is set on the Parent Opportunity.');

        for(Cloned_Object_Fields_Default_Value__mdt mtd : defaultOppMeta) {
            if(mtd.Default_Value_TEXT__c != null) {
                System.assertEquals(mtd.Default_Value_TEXT__c, (String)clonedOpp.get(mtd.Cloned_Field__r.QualifiedApiName), 'Invalid default value for '+mtd.Cloned_Field__r.QualifiedApiName);
            }
        }
    }

    @isTest
    private static void testOnUpdateCloningOpportunitiesAllTriggersBypassed(){
        TestUtils.deleteCustomSettings();
        TestDataFactory.insertCustomSettings(true, false, true);

        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];

        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        Test.startTest();

        update opp;

        Test.stopTest();

        List<Opportunity> clonedOpps = [SELECT Id, Parent_Opportunity__c FROM Opportunity WHERE Parent_Opportunity__c IN: opps LIMIT 1];
        System.assertEquals(0, clonedOpps.size(), 'Opportunity record must not have been cloned.');
    }

    @isTest
    private static void testOnUpdateCloningOpportunitiesTriggersBypassedButOpportunityTriggerEnabled(){
        TestUtils.deleteCustomSettings();
        TestDataFactory.insertCustomSettings(true, true, true);

        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 1];

        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        Test.startTest();

        update opp;

        Test.stopTest();

        String oppObjName = Schema.SObjectType.Opportunity.getName();
        
        List<Cloned_Object_Fields_Default_Value__mdt> defaultOppMeta = SObjectCloner.queryCustomMetadataTypes(OPPORTUNITY_CLONING_PROCESS, oppObjName);
        Set<String> fieldNames = new Set<String>();
        for(Cloned_Object_Fields_Default_Value__mdt mtd : defaultOppMeta) {
            fieldNames.add(mtd.Cloned_Field__r.QualifiedApiName);
        }

        Opportunity clonedOpp = Database.query('SELECT '+String.join(new List<String>(fieldNames), ',')+',Parent_Opportunity__c FROM Opportunity WHERE Parent_Opportunity__c IN: opps LIMIT 1');
        System.assertNotEquals(null, clonedOpp, 'Opportunity record must be cloned.');
        System.assertEquals(opps[0].Id, clonedOpp.Parent_Opportunity__c, 'Invalid Parent Opportunity is set on the Parent Opportunity.');

        for(Cloned_Object_Fields_Default_Value__mdt mtd : defaultOppMeta) {
            if(mtd.Default_Value_TEXT__c != null) {
                System.assertEquals(mtd.Default_Value_TEXT__c, (String)clonedOpp.get(mtd.Cloned_Field__r.QualifiedApiName), 'Invalid default value for '+mtd.Cloned_Field__r.QualifiedApiName);
            }
        }
    }

    @isTest
    private static void testOnBulkUpdateOpportunities() {
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 50];

        for(Opportunity opp : opps) {
            opp = TestUtils.closeWinOpportunity(opp);
        }
        
        Test.startTest();

        update opps;

        Test.stopTest();

        List<Opportunity> clonedOpps = [SELECT Id, StageName FROM Opportunity WHERE Parent_Opportunity__c IN: opps];
        System.assertEquals(5, clonedOpps.size(), '5 Opportunity records must be cloned.');
    }

    @isTest
    private static void testOnUpdateRenewalOpportunityLostGracePeriodRemoval(){
        Opportunity opp = [SELECT Id, Name, AccountId FROM Opportunity ORDER BY Name LIMIT 1];

        opp.StageName = OPPORTUNITY_STAGE_LOST;
        opp.Closed_Reasons__c = 'No Budget';
        opp.Type = OPPORTUNITY_RENEWAL_TYPE;
        List<Opportunity_Asset__c> oppAssets = [SELECT Asset__r.Grace_Period__c FROM Opportunity_Asset__c WHERE Opportunity__c = :opp.Id];
        System.assertEquals(
            1, 
            oppAssets.size(), 
            'Only one Opportunity Asset should exist against the Opportunity'
        );
        System.assertEquals(
            GRACE_PERIOD, 
            oppAssets[0].Asset__r.Grace_Period__c, 
            'Grace Period should exist before renewal Opportunity is lost'
        );

        Test.startTest();

        update opp;

        Test.stopTest();
        oppAssets = [SELECT Asset__r.Grace_Period__c FROM Opportunity_Asset__c WHERE Opportunity__c = :opp.Id];

        System.assertEquals(
            1, 
            oppAssets.size(), 
            'Only one Opportunity Asset should exist against the Opportunity'
        );
        System.assertEquals(
            0, 
            oppAssets[0].Asset__r.Grace_Period__c, 
            'Grace Period should be removed from related Asset when renewal Opportunity is lost'
        );
    }

    @isTest
    private static void testOnUpdateRenewalOpportunityLostGracePeriodRemovalBulk(){
        List<Opportunity> opps = [SELECT Id, Name, AccountId FROM Opportunity ORDER BY Name LIMIT :BULK_RECORD_AMOUNT];
        for (Opportunity opp : opps) {
            opp.Type = OPPORTUNITY_RENEWAL_TYPE;
            opp.StageName = OPPORTUNITY_STAGE_LOST;
            opp.Closed_Reasons__c = 'No Budget';
        }

        Test.startTest();

        update opps;

        Test.stopTest();
        List<Opportunity_Asset__c> oppAssets = [SELECT Asset__r.Grace_Period__c FROM Opportunity_Asset__c WHERE Opportunity__c IN :opps];

        System.assertEquals(
            BULK_RECORD_AMOUNT, 
            oppAssets.size(), 
            'There should be the same number of Opportunity Asset should exist as the number of Opportunities'
        );
        for (Opportunity_Asset__c oppAsset : oppAssets) {
            System.assertEquals(
                0, 
                oppAssets[0].Asset__r.Grace_Period__c, 
                'Grace Period should be removed from related Asset when renewal Opportunity is lost'
            );
        }
        
    }

    @isTest
    private static void testOnUpdateRenewalOpportunityLostGracePeriodRemovalFailure(){
        Opportunity opp = [SELECT Id, Name, AccountId FROM Opportunity ORDER BY Name LIMIT 1];
        
        opp.StageName = OPPORTUNITY_STAGE_LOST;
        opp.Closed_Reasons__c = 'No Budget';
        opp.Type = OPPORTUNITY_RENEWAL_TYPE;
        
        List<Opportunity_Asset__c> oppAssets = [SELECT Asset__r.Grace_Period__c FROM Opportunity_Asset__c WHERE Opportunity__c = :opp.Id];
        System.assertEquals(
            1, 
            oppAssets.size(), 
            'Only one Opportunity Asset should exist against the Opportunity'
        );
        System.assertEquals(
            GRACE_PERIOD, 
            oppAssets[0].Asset__r.Grace_Period__c, 
            'Grace Period should exist before renewal Opportunity is lost'
        );

        AssetGenerationHelper.simulateDMLException = true;

        Test.startTest();

        update opp;

        Test.stopTest();

        oppAssets = [SELECT Asset__r.Grace_Period__c FROM Opportunity_Asset__c WHERE Opportunity__c = :opp.Id];
        System.assertEquals(
            1, 
            oppAssets.size(), 
            'Only one Opportunity Asset should exist against the Opportunity'
        );
        System.assertEquals(
            GRACE_PERIOD, 
            oppAssets[0].Asset__r.Grace_Period__c, 
            'Grace Period should be the same as before when there is an error'
        );

        
    }

    @isTest
    private static void testGenerateOpportunityContactRoles(){

        List<Opportunity> opps = [SELECT Id, Name, AccountId, (SELECT Id, Role, ContactId FROM OpportunityContactRoles) FROM Opportunity ORDER BY Name LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(true, 'New Test', opps[0].AccountId, OPPORTUNITY_RENEWAL_TYPE, System.today().addMonths(1), 'United Kingdom', 12, false);
        
        Map<Id,Opportunity> opportunities = new Map<Id, Opportunity>{
            opps[0].Id => opp

        };

        Test.startTest();

        OpportunityTriggerHelper.renewalOpportunities = opportunities;
        OpportunityTriggerHelper.generateOpportunityContactRoles();
        
        Test.stopTest();

        List<OpportunityContactRole> oppRoles = [SELECT Id, Role, ContactId FROM OpportunityContactRole WHERE OpportunityId = :opp.Id];
        System.assertEquals(2, oppRoles.size(), 'Opportunity Contact Roles were not cloned.');

        //validate all opportunity contact role is correctly copied
        Set<Id> oldContactIds = new Set<Id>();
        Set<String> roles = new Set<String>();

        for(OpportunityContactRole oppRole : opps[0].OpportunityContactRoles) {
            oldContactIds.add(oppRole.ContactId);
            roles.add(oppRole.Role);
        }

        for(OpportunityContactRole oppContRole : oppRoles) {
            System.assert(roles.contains(oppContRole.Role), 'Opportunity Contact Role is not copied.');
            System.assert(oldContactIds.contains(oppContRole.ContactId), 'Opportunity Contact is not copied.');
        }
    }

    private static void testGenerateOpportunityContactRolesErrorHandling(){

        List<Opportunity> opps = [SELECT Id, Name, AccountId FROM Opportunity ORDER BY Name LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(true, 'New Test', opps[0].AccountId, OPPORTUNITY_RENEWAL_TYPE, System.today().addMonths(1), 'United Kingdom', 12, false);
        
        Map<Id,Opportunity> opportunities = new Map<Id, Opportunity>{
            opps[0].Id => opp

        };

        User readOnlyUser = TestDataFactory.createUser(true, 'Read Only', 'test@test.com', 'avunittestxzy@avunittestxzy.com');

        Test.startTest();
    
        System.runAs(readOnlyUser) {
            OpportunityTriggerHelper.renewalOpportunities = opportunities;
            OpportunityTriggerHelper.generateOpportunityContactRoles();
        }
        
        Test.stopTest();

        List<OpportunityContactRole> oppRoles = [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :opp.Id];
        System.assertEquals(0, oppRoles.size(), 'Opportunity Contact Roles should not be created.');
    }

    @isTest
    private static void testGenerateOpportunityProducts(){
        List<Opportunity> opps = [SELECT Id, Name, AccountId, (SELECT Id, Product2Id FROM OpportunityLineItems) FROM Opportunity ORDER BY Name LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(true, 'New Test', opps[0].AccountId, OPPORTUNITY_RENEWAL_TYPE, System.today().addMonths(1), 'United Kingdom', 12, false);
        
        Map<Id,Opportunity> opportunities = new Map<Id, Opportunity>{
            opps[0].Id => opp

        };

        Test.startTest();

        OpportunityTriggerHelper.renewalOpportunities = opportunities;
        OpportunityTriggerHelper.generateOpportunityProducts();
        
        Test.stopTest();

        List<OpportunityLineItem> oppProducts = [SELECT Id, Product2Id FROM OpportunityLineItem WHERE OpportunityId = :opp.Id];
        System.assertEquals(2, oppProducts.size(), 'Opportunity line items were not cloned.');

        //validate all Products are correctly copied
        Set<Id> oldProductIds = new Set<Id>();
        for(OpportunityLineItem oppProduct : opps[0].OpportunityLineItems) {
            oldProductIds.add(oppProduct.Product2Id);
        }

        for(OpportunityLineItem oppProduct : oppProducts) {
            System.assert(oldProductIds.contains(oppProduct.Product2Id), 'Opportunity Product is not copied.');
        }
    }

    @isTest
    private static void testOnUpdateCloningOpportunitiesException(){
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 50];

        Opportunity opp = TestUtils.closeWinOpportunity(opps[0]);

        User readOnlyUser = TestDataFactory.createUser(true, 'Read Only', 'test@test.com', 'avunittestxzy@avunittestxzy.com');

        Test.startTest();

        System.runAs(readOnlyUser) {
            try {
                update opp;
                System.assert(false, 'Exception is expected to be thrown from above line.');
            } catch (Exception ex) {
                System.assertNotEquals(null, ex, 'Exception should be populated');
            }
            
        }

        Test.stopTest();

        Opportunity[] clonedOpp = [SELECT Id, StageName FROM Opportunity WHERE Parent_Opportunity__c IN: opps];
        System.assertEquals(0, clonedOpp.size(), 'Opportunity record must not be cloned.');
    }

    @isTest
    private static void testPrimaryContactValidationWithNonPrimaryContact() {

        Account account = TestDataFactory.createAccount(true, 'Test', 'abc Street', 'abc City');
        Contact contact = TestDataFactory.createContact(true, account.Id, 'Test Acc', 'TEST', 'test@test.com');
        Opportunity opp = TestDataFactory.createOpportunity(true, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        TestDataFactory.createOpportunityContactRole(true, contact.Id, opp.Id, 'Business User', false);

        opp = TestUtils.closeWinOpportunity(opp);

        TestUtils.deleteCustomSettings();
        TestDataFactory.insertCustomSettings(false, false, false);
        
        Test.startTest();

        try {
            update opp;
            System.assert(false, 'Exception is expected to be thrown');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex, 'Exception cannot be null');
        }
        
        Test.stopTest();
    }

    @isTest
    private static void testPrimaryContactValidationWithNoRelatedContacts() {

        Account account = TestDataFactory.createAccount(true, 'Test', 'abc Street', 'abc City');
        Opportunity opp = TestDataFactory.createOpportunity(true, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        
        opp = TestUtils.closeWinOpportunity(opp);

        TestUtils.deleteCustomSettings();
        TestDataFactory.insertCustomSettings(false, false, false);

        Test.startTest();

        try {
            update opp;
            System.assert(false, 'Exception is expected to be thrown');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex, 'Exception cannot be null');
        }
        
        Test.stopTest();
    }

    @isTest
    private static void testPrimaryContactValidationWithNoRelatedContactsOnCreation() {
        TestUtils.deleteCustomSettings();
        TestDataFactory.insertCustomSettings(false, false, false);
        Account account = TestDataFactory.createAccount(true, 'Test', 'abc Street', 'abc City');
        Opportunity opp = TestDataFactory.createOpportunity(false, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        
        opp = TestUtils.closeWinOpportunity(opp);

        Test.startTest();

        try {
            insert opp;
            System.assert(false, 'Exception is expected to be thrown');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex, 'Exception cannot be null');
        }
        
        Test.stopTest();
    }

    @isTest
    private static void testPrimaryContactValidationRelatedPrimaryContact() {
        Account account = TestDataFactory.createAccount(true, 'Test', 'abc Street', 'abc City');
        Contact contact = TestDataFactory.createContact(true, account.Id, 'Test Acc', 'TEST', 'test@test.com');
        Opportunity opp = TestDataFactory.createOpportunity(true, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        TestDataFactory.createOpportunityContactRole(true, contact.Id, opp.Id, 'Business User', true);

        opp = TestUtils.closeWinOpportunity(opp);

        Test.startTest();

        update opp;
    
        Test.stopTest();

        opp = [SELECT Id, StageName FROM Opportunity WHERE Id =: opp.Id LIMIT 1];

        System.assertEquals(OPPORTUNITY_STAGE_WON, opp.StageName, 'Opportunity stage must be set to '+OPPORTUNITY_STAGE_WON);
    }

    @isTest
    private static void testCreationOfTrialProductsOnInsert() {

        TestDataFactory.activateStandardPricebook();
        Account a = [SELECT Id FROM Account LIMIT 1];
        Product2 product = TestDataFactory.createProductWithPriceBook('Test', 'TESTPRODCODE', 0, true);
        Opportunity opp = TestDataFactory.createOpportunity(false, 'Test Trial', a.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 1, false);
        opp.Product_Code__c = product.ProductCode;
        
        Test.startTest();

        insert opp;

        Test.stopTest();

        Opportunity trialOpp = [SELECT Id, RecordTypeId, (SELECT Id FROM OpportunityLineItems) FROM Opportunity WHERE Parent_Opportunity__c = :opp.Id];
        System.assertNotEquals(null, trialOpp, 'Trial Opportunity must be created');
        System.assertEquals(TrialHelper.trialRecordTypeId, trialOpp.RecordTypeId);
        System.assertEquals(1, trialOpp.OpportunityLineItems.size(), 'Opportunity Line Item must be created.');
    }


    @isTest
    private static void testWithoutProductCodeOnInsert() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        Product2 product = TestDataFactory.createProductWithPriceBook('Test', 'TESTPRODCODE', 0, true);
        Opportunity opp = TestDataFactory.createOpportunity(false, 'Test Trial', a.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 1, false);
        opp.Product_Code__c = null;
        
        Test.startTest();

        insert opp;

        Test.stopTest();

        List<Opportunity> trialOpp = [SELECT Id, RecordTypeId, (SELECT Id FROM OpportunityLineItems) FROM Opportunity WHERE Parent_Opportunity__c = :opp.Id];
        System.assertEquals(0, trialOpp.size(), 'Trial Opportunity must not be created');
    }

    @isTest
    private static void testCreationOfTrialProductsOnInsertBulk() {
        TestDataFactory.activateStandardPricebook();
        Account a = [SELECT Id FROM Account LIMIT 1];
        Product2 product = TestDataFactory.createProductWithPriceBook('Test', 'TESTPRODCODE', 0, true);
        List<Opportunity> opps = TestDataFactory.createOpportunities(false, 5, a.Id);
        for(Opportunity opp : opps) {
            opp.Product_Code__c = product.ProductCode;
        }
        
        Test.startTest();

        insert opps;

        Test.stopTest();

        List<Opportunity> trialOpps = [SELECT Id, RecordTypeId, (SELECT Id FROM OpportunityLineItems) FROM Opportunity WHERE Parent_Opportunity__c IN :opps];
        System.assertEquals(5, trialOpps.size(), 'Trial Opportunity must be created');

        for(Opportunity trialOpp : trialOpps) {
            System.assertEquals(TrialHelper.trialRecordTypeId, trialOpp.RecordTypeId);
            System.assertEquals(1, trialOpp.OpportunityLineItems.size(), 'Opportunity Line Item must be created.');
        }
    }

    @isTest
    private static void testPopulatingStandardPricebookOnInsert() {
        TestDataFactory.activateStandardPricebook();
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(false, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);

        Test.startTest();

        insert opp;

        Test.stopTest();

        opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id =: opp.Id LIMIT 1];
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true LIMIT 1];
        System.assertEquals(pb.Id, opp.Pricebook2Id, 'Invalid Pricebook Id');
    }

    @isTest
    private static void testAssigningApproverOnInsert() {

        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(false, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        
        User approver = TestDataFactory.createUser(true, 'System Administrator', 'test@test.com', 'avunittdsfdsf@testnbmq.com');
        List<Opportunity_Approvers__mdt> approversMetadata = TestDataFactory.getApproverMetaData(approver);

        Test.startTest();

        OpportunityTriggerHelper.approversMetadata = approversMetadata;
        insert opp;

        Test.stopTest();

        opp = [SELECT Id, Country_Director_Approver__c, Regional_Director_Approver__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        System.assertEquals(approver.Id, opp.Country_Director_Approver__c);
        System.assertEquals(approver.Id, opp.Regional_Director_Approver__c);
    }

    @isTest
    private static void testAssigningApproverOnUpdate() {

        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(true, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'Germany', 12, false);

        User approver = TestDataFactory.createUser(true, 'System Administrator', 'test@test.com', '987testAV@6565testav.com');
        List<Opportunity_Approvers__mdt> approversMetadata = TestDataFactory.getApproverMetaData(approver);
        
        Test.startTest();

        OpportunityTriggerHelper.approversMetadata = approversMetadata;
        opp.Sales_Country__c = 'United Kingdom';
        update opp;

        Test.stopTest();

        opp = [SELECT Id, Country_Director_Approver__c, Regional_Director_Approver__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        System.assertEquals(approver.Id, opp.Country_Director_Approver__c);
        System.assertEquals(approver.Id, opp.Regional_Director_Approver__c);
    }

    @isTest
    private static void testAssignApprovers() {

        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(false, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        
        User approver = TestDataFactory.createUser(true, 'System Administrator', 'test@test.com', 'avunittdsfdsf@testnbmq.com');
        List<Opportunity_Approvers__mdt> approversMetadata = TestDataFactory.getApproverMetaData(approver);

        Test.startTest();

        OpportunityTriggerHelper.approversMetadata = approversMetadata;
        OpportunityTriggerHelper.assignApprovers(new List<Opportunity>{opp}, new Set<String>{'United Kingdom'});

        Test.stopTest();

        System.assertEquals(approver.Id, opp.Country_Director_Approver__c);
        System.assertEquals(approver.Id, opp.Regional_Director_Approver__c);
    }

    @isTest
    private static void testAssignApproversWithoutCountry() {

        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opp = TestDataFactory.createOpportunity(false, 'TEST', account.Id, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'Germany', 12, false);
        
        User approver = TestDataFactory.createUser(true, 'System Administrator', 'test@test.com', 'avunittdsfdsf@testnbmq.com');
        List<Opportunity_Approvers__mdt> approversMetadata = TestDataFactory.getApproverMetaData(approver);

        Test.startTest();

        OpportunityTriggerHelper.approversMetadata = approversMetadata;
        OpportunityTriggerHelper.assignApprovers(new List<Opportunity>{opp}, new Set<String>{'Germany'});

        Test.stopTest();

        System.assertEquals(null, opp.Country_Director_Approver__c);
        System.assertEquals(null, opp.Regional_Director_Approver__c);
    }
    
    @isTest
    private static void testUpSellOpportunityCloseErrorMessage() {

        Opportunity currentOpp = [SELECT Id, AccountId, Company__c, PriceBook2Id, (SELECT Id, Product2Id FROM OpportunityLineItems LIMIT 1), (SELECT Id, ContactId FROM OpportunityContactRoles LIMIT 1) FROM Opportunity LIMIT 1];
        
        TestUtils.closeWinOpportunity(currentOpp);
        update currentOpp;

        Opportunity upSellOpp = TestDataFactory.createOpportunity(false, 'TEST', currentOpp.accountId, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today().addMonths(1), 'United Kingdom', 12, false);
        upSellOpp.Company__c = currentOpp.Company__c;
        upSellOpp.PriceBook2Id = currentOpp.PriceBook2Id;
        upSellOpp.Type = OpportunityTriggerHandler.OPPORTUNITY_UPSELL_TYPE;
        insert upSellOpp;

        OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = upSellOpp.Id, Product2Id = currentOpp.OpportunityLineItems[0].Product2Id, UnitPrice = 200, Quantity = 10);
        insert line1;

        TestDataFactory.createOpportunityContactRole(true, currentOpp.OpportunityContactRoles[0].ContactId, upSellOpp.Id, 'Business User', true);
    
        Test.startTest();

        try {
            TestUtils.deleteCustomSettings();
            upSellOpp = TestUtils.closeWinOpportunity(upSellOpp);
            update upSellOpp;

            System.assert(false, 'Error message expected: Opportunity End Date must match Asset End Date.');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex);
        }

        Test.stopTest();
    }

    @isTest
    private static void testNewBusinessOpportunityCloseErrorMessage() {
        
        
        Opportunity currentOpp = [SELECT Id, AccountId, Company__c, PriceBook2Id, End_Date__c, (SELECT Id, Product2Id FROM OpportunityLineItems LIMIT 1), (SELECT Id, ContactId FROM OpportunityContactRoles LIMIT 1) FROM Opportunity LIMIT 1];
        TestUtils.closeWinOpportunity(currentOpp);
        update currentOpp;

        Opportunity newBisOpp = TestDataFactory.createOpportunity(false, 'TEST', currentOpp.accountId, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today().addMonths(1), 'United Kingdom', 12, false);
        newBisOpp.Company__c = currentOpp.Company__c;
        newBisOpp.PriceBook2Id = currentOpp.PriceBook2Id;
        newBisOpp.Type = OpportunityTriggerHandler.OPPORTUNITY_NEW_BUSINESS_TYPE;
        insert newBisOpp;

        OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = newBisOpp.Id, Product2Id = currentOpp.OpportunityLineItems[0].Product2Id, UnitPrice = 200, Quantity = 10);
        insert line1;

        TestDataFactory.createOpportunityContactRole(true, currentOpp.OpportunityContactRoles[0].ContactId, newBisOpp.Id, 'Business User', true);
    
        Test.startTest();
        try {
            TestUtils.deleteCustomSettings();
            newBisOpp = TestUtils.closeWinOpportunity(newBisOpp);
            update newBisOpp;
            
            System.assert(false, 'Error message expected: ' + System.Label.New_Business_With_Active_Asset + '.');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex);
        }
        Test.stopTest();
    }

    @IsTest
    private static void testUpSellOpportunityClose() {

        Opportunity currentOpp = [SELECT Id, AccountId, Company__c, PriceBook2Id, End_Date__c, (SELECT Id, Product2Id FROM OpportunityLineItems LIMIT 1), (SELECT Id, ContactId FROM OpportunityContactRoles LIMIT 1) FROM Opportunity LIMIT 1];
        
        TestUtils.closeWinOpportunity(currentOpp);
        update currentOpp;

        Opportunity upSellOpp = TestDataFactory.createOpportunity(false, 'TEST', currentOpp.accountId, OPPORTUNITY_FIRST_CONTACT_STAGE, System.today(), 'United Kingdom', 12, false);
        upSellOpp.Company__c = currentOpp.Company__c;
        upSellOpp.PriceBook2Id = currentOpp.PriceBook2Id;
        upSellOpp.Type = OpportunityTriggerHandler.OPPORTUNITY_UPSELL_TYPE;
        insert upSellOpp;

        OpportunityLineItem line1 = new OpportunityLineItem(OpportunityId = upSellOpp.Id, Product2Id = currentOpp.OpportunityLineItems[0].Product2Id, UnitPrice = 200, Quantity = 10);
        insert line1;

        TestDataFactory.createOpportunityContactRole(true, currentOpp.OpportunityContactRoles[0].ContactId, upSellOpp.Id, 'Business User', true);
    
        Test.startTest();

        upSellOpp = TestUtils.closeWinOpportunity(upSellOpp);
        upSellOpp.End_Date__c = currentOpp.End_Date__c;
        update upSellOpp;

        Test.stopTest();
        Opportunity renewalOpp = [SELECT Id, (SELECT Id, Quantity, Product2Id FROM OpportunityLineItems) FROM Opportunity WHERE Parent_Opportunity__c = :currentOpp.Id LIMIT 1];
        Decimal quantity = 0;
        for(OpportunityLineItem renewalLineItem : renewalOpp.OpportunityLineItems) {
            if(renewalLineItem.Product2Id == currentOpp.OpportunityLineItems[0].Product2Id) {
                quantity = renewalLineItem.Quantity;
                break;
            }
        }
        System.assertEquals(12, quantity, 'Renewal OpportunityLineItem should be total of existing quantity and up sell quantity.');
    }
}