/**
* @author Sapienti7
* @date 20/12/2018
* @description This virtual class implements the ‘ITriggerController’ interface.  
               All the trigger handler class should extend the ‘TriggerHandlerTemplate’ virtual class. 
*/
public virtual class TriggerHandlerTemplate implements ITriggerController {

    public class NotImplementedException extends Exception{} 

    protected boolean triggerIsOff;

    /**
    * @author Sapienti7
    * @date 20/12/2018
    * @description This virtual method will be optionally overriden in the trigger controller class.
                   By default it will invoke methods (i.e. onBeforeInsert, onAfterUpdate etc.) depending on the trigger's events.
    */
    public virtual void mainEntry(String triggerObject, Boolean isBefore, 
                                 Boolean isDelete, Boolean isAfter,
                                 Boolean isInsert, Boolean isUpdate, 
                                 Boolean isUndelete, Boolean isExecuting,
                                 List<SObject> newlist, Map<Id, SObject> newmap,
                                 List<SObject> oldlist, Map<Id, SObject> oldmap){
        if (getTriggerIsOff()) {
            return;
        }

        triggerObject = triggerObject + TriggerConstant.TRIGGER_HANDLER_STRING;
        Type tHandler = Type.forName(triggerObject);

        if(isBefore) {

            if(isInsert){
                onBeforeInsert(newlist);
            } else if(isUpdate){
                onBeforeUpdate(newlist, newmap, oldlist, oldmap);
            } else if(isDelete){
                onBeforeDelete(oldlist, oldmap);
            }

        } else if(isAfter) {

            if(isInsert){
                onAfterInsert(newlist, newmap);
            } else if(isUpdate){
                onAfterUpdate(newlist, newmap, oldlist, oldmap);
            } else if(isDelete){
                onAfterDelete(oldlist, oldmap);
            } else if(isUndelete){
                onAfterUndelete(newlist, newmap);
            }
        }
    }

    /**
    * @author Sapienti7
    * @date 20/12/2018
    * @description This virtual method will be optionally overriden in the trigger controller class.
                   By default it will create an instance of the mainEntry() in the Trigger
    */
    public virtual void inProgressEntry(String triggerObject, Boolean isBefore, 
                                        Boolean isDelete, Boolean isAfter,
                                        Boolean isInsert, Boolean isUpdate, 
                                        Boolean isUndelete, Boolean isExecuting,
                                        List<SObject> newlist, Map<Id, SObject> newmap,
                                        List<SObject> oldlist, Map<Id, SObject> oldmap) {
        if (getTriggerIsOff()) {
            return;
        }

        Type tHandler = Type.forName(triggerObject);

        ITriggerController activeFunction = (ITriggerController)tHandler.newInstance();

        //Update the dispatcher function so that it references the new object
        TriggerDispatcher.activeFunction = activeFunction;

        activeFunction.mainEntry(triggerObject, isBefore, isDelete, isAfter, isInsert, isUpdate, isUndelete, isExecuting,
                    newlist, newmap, oldlist, oldmap);

        //Reset the active function so that it points to the current class
        TriggerDispatcher.activeFunction = this;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on before insert trigger event
	*/
    public virtual void onBeforeInsert(List<SObject> newlist){
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on before update trigger event
	*/
    public virtual void onBeforeUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap) {
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on before delete trigger event
	*/
    public virtual void onBeforeDelete(List<SObject> oldlist, Map<Id, SObject> oldmap){
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after insert trigger event
	*/
    public virtual void onAfterInsert(List<SObject> newlist, Map<Id, SObject> newmap){
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after update trigger event
	*/
    public virtual void onAfterUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap) {
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after delete trigger event
	*/
    public virtual void onAfterDelete(List<SObject> oldlist, Map<Id, SObject> oldmap){
        return;
    }

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description This method must be overriden in the sub class i.e. trigger handler to perform 
                    all the required actions on after undelete trigger event
	*/
    public virtual void onAfterUndelete(List<SObject> newlist, Map<Id, SObject> newmap){
        return;
    }

    /**
    * @author Sapienti7
    * @date 09/04/2019
    * @description This method will check if the trigger framework should be bypassed. 
    *              This can be overridden in the specific implementation.
    */
    protected virtual Boolean getTriggerIsOff() {
        if(triggerIsOff == null) {
            Bypass_Configuration__c bypassConfig = Bypass_Configuration__c.getInstance();
            triggerIsOff = bypassConfig == null ? false : bypassConfig.IsTriggerFrameworkBypassed__c;
        }
        return triggerIsOff;
    } 
}