/**
* @author 		Sapienti7
* @date 		10/09/2019
* @description  Useful Test methods to be called during test context
*/
@isTest
public class TestUtils {
    
    private static final string OPPORTUNITY_STAGE_WON = System.Label.OpportunityWonStage;

    public static Opportunity closeWinOpportunity(Opportunity opp) {
		opp.StageName = OPPORTUNITY_STAGE_WON;
		opp.Closed_Reasons__c = 'Product';
		opp.Signature_Status__c = 'Signed by Customer';
		opp.Billing_Frequency__c = 'Monthly';
		return opp;
	}

	public static void deleteCustomSettings() {
		Bypass_Configuration__c bypassConfig = [SELECT Id FROM Bypass_Configuration__c];
		delete bypassConfig;
	}
}