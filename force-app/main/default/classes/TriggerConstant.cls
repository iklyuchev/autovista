/**
* @author Sapienti7
* @date 20/12/2018
* @description Class used to hold all trigger related constants
*/

public class TriggerConstant {
    
    public final static String TRIGGER_HANDLER_STRING = 'TriggerHandler';
    public final static String TRIGGER_NAME_CONTACT = 'Contact';
    public final static String TRIGGER_NAME_OPPORTUNITY = 'Opportunity';
    public final static String TRIGGER_NAME_ASSET = 'Asset';
    public final static String TRIGGER_NAME_ACCOUNT = 'Account';
    public final static String TRIGGER_NAME_TRANSACTION = 'Transaction';
    public final static String TRIGGER_NAME_TRANSACTION_LINEITEM = 'c2g__codaTransactionLineItem__c';
}