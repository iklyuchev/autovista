public class DeleteIntegrationLogBatch implements Database.Batchable<SObject> {

    public Database.QueryLocator start(Database.BatchableContext BC){
        Date threeMonthsBefore = System.today().addMonths(-3);
        return Database.getQueryLocator('SELECT Id FROM Integration_Log__c WHERE CreatedDate <= :threeMonthsBefore ORDER BY Target_ID__c');
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){
        this.deleteIntegrationLogs(scope);
    }

    @TestVisible
    private void deleteIntegrationLogs(List<Integration_Log__c> logs) {
        Log.push('DeleteIntegrationLogBatch.execute');
        try {
            delete logs;
            //empty recycle bin so that these records are permanently deleted.
            Database.emptyRecycleBin(logs);
        } catch (Exception ex) {
            Log.error(String.valueOf(ex.getMessage()+'-'+ex.getStackTraceString()));
            Log.pop();
            Log.emit();
        }
    }

    public void finish(Database.BatchableContext BC){}
}