/**
* @author Sapienti7
* @date 31/12/2018
* @description Simple in memory low cpu logging
*/
public class Log {

    @TestVisible private static LogScope current;
    @TestVisible private static LogScope root; 
    @TestVisible private static String currentSeverity;

    public enum Severity { warning, info, debug, error }
    private static final String ERROR = 'Error';
    private static final String WARNING = 'Warning';
    private static final String INFO = 'Info';
    private static final String DEBUG = 'Debug';  

    private static final Integer DML_IDX = 0;
    private static final Integer SOQL_IDX = 1;
    private static final Map<Integer, String> LIMITS_BY_INDEX = 
        new Map<Integer, String> { DML_IDX => 'DML', SOQL_IDX => 'SQL'};
        
    static {
        reset();
    }

    /**
     * Push this named scope onto the log stack
     **/    
    public static void push(String scope) {
        LogScope newScope = new LogScope();
        newScope.scope = scope;
        newScope.limits = new List<Integer> { 
            Limits.getDMLStatements(), 
            Limits.getQueries() };
        newScope.parent = Log.current;
        Log.current.logEntries.add(newScope);
        Log.current = newScope;      
        System.debug('Ivan push Log.current : ' + Log.current);  
    }
    
    /**
     * Add the given message to the scope last pushed
     **/
    public static void info(String message) {
        Log.current.logEntries.add(createLogEntry(Severity.info, message)); 
        Log.current.severity.add(INFO);        
    }

    public static void debug(String message) {
        Log.current.logEntries.add(createLogEntry(Severity.debug, message));
        Log.current.severity.add(DEBUG);       
    }

    public static void warn(String message) {
        Log.current.logEntries.add(createLogEntry(Severity.warning, message));
        Log.current.severity.add(WARNING);          
    }

    public static void error(String message) {
        Log.current.logEntries.add(createLogEntry(Severity.error, message));
        Log.current.severity.add(ERROR);        
    }

    public static void error(Exception e) {
        Log.current.logEntries.add(createLogEntry(Severity.error, e.getMessage()));
        Log.current.logEntries.add('Stack trace: ' + e.getStackTraceString());
        Log.current.severity.add(ERROR);               
    }

    public static void error(String message, Exception e) {
        Log.current.logEntries.add(createLogEntry(Severity.error, message + ' : ' + e.getMessage()));
        Log.current.logEntries.add('Stack trace: ' + e.getStackTraceString());
        Log.current.severity.add(ERROR);           
    }

    private static String createLogEntry(Severity s, String message){
        String severityString = String.valueOf(s).toUpperCase();
        return getTimeNow() + '  ' + severityString + ' - ' + message;
    }

    /**
     * Pop the current scope
     **/
    public static void pop() {
        System.debug('Ivan DML_IDX : ' + DML_IDX);
        System.debug('Ivan pop Log.current : ' + Log.current);
        Log.current.limits = new List<Integer> { 
            Limits.getDMLStatements() - Log.current.limits[DML_IDX], 
            Limits.getQueries() - Log.current.limits[SOQL_IDX]};
        Log.current = Log.current.parent;           
    }

    /**
     * Flush the log entries and serialize 
     **/    
    public static String flush() {
        List<String> logLines = new List<String>();
        logLines.add(Log.root.scope);
        Log.root.flush(logLines, 1);
        reset();
        return String.join(logLines, '/n');        
    }
    
    /**
     * Emits the current log state to the Log__e platform event
     **/
    public static void emit() {
        
        // Log title is primarly present to allow filtering
        String title = Log.root.scope;
        Integer dml = -1;
        Integer soql = -1;
        currentSeverity = currentSeverity == null ? INFO : currentSeverity;

        if(Log.root.logEntries.size()>0) {
            Object top = Log.root.logEntries[0];
            if(top instanceof LogScope) {
                LogScope logScope = (LogScope) top;
                title = logScope.scope;                
                dml = logScope.limits[DML_IDX];
                soql = logScope.limits[SOQL_IDX];
                if(logScope.severity.contains(ERROR)){
                    currentSeverity = ERROR;
                } else if(logScope.severity.contains(WARNING)){
                    currentSeverity = WARNING;
                }

            } else if (top instanceof String) {
                title = (String) top;                
            }
        }
       
        // Emit the log data via Platform Events
        EventBus.publish(new Log__e(
                Title__c = title,
                DML__c = dml,
                SOQL__c = soql,
                Severity__c = currentSeverity,
                Data__c = flush()));
    }    
    
    /**
     * Resets the log state
     **/
    private static void reset() {
        root = current = new LogScope();
        current.scope = getDateTimeNow();       
    }
    
    private static String getDateTimeNow(){
        DateTime now = DateTime.now();
        return String.valueOf(now+'.'+now.millisecond());
    }

    private static String getTimeNow(){
        DateTime now = DateTime.now();
        String timeNow = String.valueOf(now.time());
        return timeNow.substring(0, timeNow.length() - 1);
    }

    /**
     * Nested log scopes
     **/
    private class LogScope {
        
        public LogScope parent;
        public String scope;
        public List<Integer>  limits;
        public List<Object> logEntries = new List<Object>();
        public Set<String> severity = new Set<String>();
               
        public void flush(List<String> logLines, Integer indent) {
            for(Object logEntry : logEntries) {

                if(logEntry instanceof LogScope) {
                    LogScope logScope = (LogScope) logEntry;
                    List<String> limitDiffs = new List<String>();
                    for(Integer limitIdx : LIMITS_BY_INDEX.keySet()) {
                        Integer limitVariance = logScope.limits[limitIdx];
                        if(limitVariance > 0) {
                            limitDiffs.add(LIMITS_BY_INDEX.get(limitIdx)+' ' + limitVariance);
                        }
                    }
                    String limits = limitDiffs.size() > 0 ? '(' + String.join(limitDiffs, ',') + ')' : '';
                    logLines.add(indent + '|' + logScope.scope + ' ' + limits); 
                    logScope.flush(logLines, indent+1);
                } else {
                    logLines.add(indent+'|'+logEntry);                    
                }
            }
        }
    }
}