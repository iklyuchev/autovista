/**
* @author       Sapienti7
* @date         03/06/2019
* @description  Utility to generate assets on Closed Won Opportunities.
*/
public without sharing class AssetGenerationHelper {


    @Testvisible
    private static Boolean simulateDMLException = false;
    @Testvisible 
    private static List<Asset_Field_Mapping__mdt> fieldmappings;
    //private static Id lastOppId;
    private static Set<String> lastOppLineItem = new Set<String>();

    private static final String OPPORTUNITY_API_NAME = Schema.SObjectType.Opportunity.getName();
    private static final String OPPORTUNITY_PRODUCT_API_NAME = Schema.SObjectType.OpportunityLineItem.getName();
    private static final String ASSET_API_NAME = Schema.SObjectType.Asset.getName();
    private static final String OPPORTUNITY_START_DATE_API_NAME = Schema.Opportunity.Start_Date__c.getDescribe().getName();
    private static final String ASSET_START_DATE_API_NAME = Schema.Asset.Current_Start_Date__c.getDescribe().getName();
    public static final String ASSET_ACTIVE_STATUS = System.Label.AssetActiveStatus;
    public static final String ASSET_INACTIVE_STATUS = System.Label.AssetInactiveStatus;

    //Map OpportunityLineItem Id to Asset instance so that Asset Id can be retrieved after insertion
    //in order to populate against Opportunity Asset record.
    @Testvisible
    private static Map<String, List<Opportunity_Asset__c>> oppAssetsToCreate;
    @Testvisible
    private static Map<String, Asset> assetsToUpsert = new Map<String, Asset>();
    
    public static void generateAssetsAndRemoveGracePeriods(Map<Id,Opportunity> closedWonOpportunities, List<Opportunity> closedLostRenewalOpportunities) {
        oppAssetsToCreate = new Map<String, List<Opportunity_Asset__c>>();
        if(closedWonOpportunities != null && closedWonOpportunities.size() > 0) {
            generateAssets(closedWonOpportunities);
        }
        if(closedLostRenewalOpportunities != null && closedLostRenewalOpportunities.size() > 0) {
            assetsToUpsert.putAll(getRelatedAssetsWithRemovedGracePeriods(closedLostRenewalOpportunities));
        }
        setupAssets();
    }

    /**
    * @description  Setup assets when opportunities are closed won. New Assets and Opportunity Assets are created according to
    *               Opportunity Line Items.
    * @param        List<Opportunity> opportunities - Closed won opportunities.
    */
    private static void generateAssets(Map<Id,Opportunity> opportunities) {
        
        //setup field mappings
        setupAssetFieldMappingMetadata();
        
        Map<String, Set<String>> queryFields = getQueryFields();

        List<Opportunity> currentOpps = getOppsWithRelatedRecords(opportunities.values(), queryFields);

        addExistingAssetsToAssetsToUpsert(currentOpps, queryFields);

        for(Opportunity opp : currentOpps) {
            
            Opportunity originalOpp = opportunities.get(opp.Id);

            syncAssets(opp, originalOpp);
            lastOppLineItem.clear();
        }
    }

    private static Map<String, Asset> getRelatedAssetsWithRemovedGracePeriods(List<Opportunity> closedLostRenewalOpportunities) {
        List<Opportunity_Asset__c> oppAssets = [SELECT Id,
                Asset__c, 
                Opportunity__c 
            FROM Opportunity_Asset__c 
            WHERE Opportunity__c IN : closedLostRenewalOpportunities];

        Map<String, Asset> assetsToUpdate = new Map<String, Asset>();
        for(Opportunity_Asset__c oppAsset : oppAssets) {
            assetsToUpdate.put(oppAsset.Asset__c, new Asset(Id = oppAsset.Asset__c, Grace_Period__c = 0));
        }
        return assetsToUpdate;
    }

    /**
    * @description  Insert new assets and update exsisting assets.
    */
    @TestVisible
    private static void setupAssets(){
        
        Log.push('OpportunityTriggerHelper.setupAssets');

        try {
            if(simulateDMLException) {
                throw new DmlException();
            }

            upsert assetsToUpsert.values();

            List<Opportunity_Asset__c> oppAssets = new List<Opportunity_Asset__c>();
            //populate Opp-Asset Asset Id
            for(String assetUniqueId : oppAssetsToCreate.keySet()) {
                for(Opportunity_Asset__c oppA : getOpportunityAssetListForAsset(assetUniqueId)) {
                    oppA.Asset__c = assetsToUpsert.get(assetUniqueId).Id;
                    oppAssets.add(oppA);
                }
            }

            insert oppAssets;

        } catch(Exception ex) {
            Log.error(String.valueOf(ex.getMessage()+'-'+ex.getStackTraceString()));
            Log.pop();
            Log.emit();
        }
    }

    /**
    * @description  Extracts the fields to be queried from the database according to custom meta data definitions.
    * @return       Map<String, Set<String>> - Object and field mappings per each object defined in the custom metadata.
    */
    @TestVisible
    private static Map<String, Set<String>> getQueryFields() {
        Map<String, Set<String>> fields = new Map<String, Set<String>>{
            OPPORTUNITY_API_NAME => new Set<String>(),
            OPPORTUNITY_PRODUCT_API_NAME => new Set<String>(),
            ASSET_API_NAME => new Set<String>()
        };

        for(Asset_Field_Mapping__mdt fieldMapping : fieldMappings) {
            fields.get(fieldMapping.Opportunity_Object__r.QualifiedApiName).add(fieldMapping.Opportunity_Field__r.QualifiedApiName);
            fields.get(fieldMapping.Asset_Object__r.QualifiedApiName).add(fieldMapping.Asset_Field__r.QualifiedApiName);
        }
        return fields;
    }

    private static List<String> getOpportunityQueryFields(Map<String, Set<String>> queryFields) {
        Set<String> oppFieldsSet = queryFields.get(OPPORTUNITY_API_NAME);
        oppFieldsSet.add(Schema.Opportunity.Type.getDescribe().getName());
        oppFieldsSet.add(Schema.Opportunity.StageName.getDescribe().getName());

        return new List<String>(oppFieldsSet);
    }

    private static List<String> getOpportunityLineQueryFields(Map<String, Set<String>> queryFields) {
        Set<String> oppLIFieldsSet = queryFields.get(OPPORTUNITY_PRODUCT_API_NAME);
        oppLIFieldsSet.add(Schema.OpportunityLineItem.OpportunityId.getDescribe().getName());
        oppLIFieldsSet.add(Schema.OpportunityLineItem.Asset_UniqueID__c.getDescribe().getName());

        return new List<String>(oppLIFieldsSet);
    }

    private static List<Opportunity> getOppsWithRelatedRecords(List<Opportunity> opportunities, Map<String, Set<String>> queryFields) {

        List<String> opportunityFields = getOpportunityQueryFields(queryFields);
        List<String> lineItemFields = getOpportunityLineQueryFields(queryFields);

        //pull Opportunity-Line Items and Opportunity-Assets
        String oppQueryStr = String.escapeSingleQuotes('SELECT Id, '+String.join(opportunityFields, ',')+','
            +' ( SELECT Id, '+String.join(lineItemFields, ',')+' FROM OpportunityLineItems), '
            +' ( SELECT Id, Asset__c, Asset__r.Status, Asset__r.Product2Id, Asset__r.Unique_Id__c FROM Opportunity_Assets__r) '
            +' FROM Opportunity WHERE ID IN :opportunities');
        
        return Database.query(oppQueryStr);
    }

    private static void addExistingAssetsToAssetsToUpsert(List<Opportunity> currentOpps, Map<String, Set<String>> queryFields) {
        Set<String> assetUniqueIds = new Set<String>();
        for(Opportunity opp : currentOpps) {
            for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                assetUniqueIds.add(oppLineItem.Asset_UniqueID__c);
            }
        }
        List<String> assetFields = new List<String>(queryFields.get(ASSET_API_NAME));

        String assetQueryStr = String.escapeSingleQuotes('SELECT Id, Status, '+String.join(assetFields, ',')
            + ' FROM Asset WHERE Unique_Id__c IN :assetUniqueIds');
            
        //System.debug('assetQueryStr: '+ assetQueryStr);
        List<Asset> existingAssets = Database.query(assetQueryStr);

        for(Asset currentAsset : existingAssets) {
            assetsToUpsert.put(currentAsset.Unique_Id__c, currentAsset);
        }
    }

    /**
    * @description  Identify assets to be created and assets to be updated per each opportunity record.
    * @param        Opportunity opp - Closed won Opportunity record with related assets and line items.
    */
    @TestVisible
    private static void syncAssets(Opportunity opp, Opportunity originalOpp) {
        Set<String> existingOpportunityAssets = new Set<String>();
        for(Opportunity_Asset__c oppAsset : opp.Opportunity_Assets__r) {
            existingOpportunityAssets.add(oppAsset.Asset__r.Unique_Id__c);
        }

        //calculate asset and opportunity assets to create/delete
        for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
            if(!existingOpportunityAssets.contains(oppLineItem.Asset_UniqueID__c)) {
                Opportunity_Asset__c oppAsset = new Opportunity_Asset__c(Opportunity__c = opp.Id);
                getOpportunityAssetListForAsset(oppLineItem.Asset_UniqueID__c).add(oppAsset);
            }

            //Link Assets to Renewal Opportunities
            Boolean renewalOpportunityExists = OpportunityTriggerHelper.renewalOpportunities != null
                && OpportunityTriggerHelper.renewalOpportunities.containsKey(oppLineItem.OpportunityId) 
                && OpportunityTriggerHelper.renewalOpportunities.get(oppLineItem.OpportunityId).Id != null;
            if(renewalOpportunityExists) {
                Opportunity_Asset__c renewalOppAsset = new Opportunity_Asset__c(Opportunity__c = OpportunityTriggerHelper.renewalOpportunities.get(oppLineItem.OpportunityId).Id);
                getOpportunityAssetListForAsset(oppLineItem.Asset_UniqueID__c).add(renewalOppAsset);
            }

            Asset asset;
            if(assetsToUpsert.containsKey(oppLineItem.Asset_UniqueID__c)) {
                asset = assetsToUpsert.get(oppLineItem.Asset_UniqueID__c);
            }
            if(validateOpportunity(opp, originalOpp, asset)) {
                asset = syncAssetFields(asset, opp, oppLineItem);
                lastOppLineItem.add(opp.Id + oppLineItem.Asset_UniqueID__c); //Mark asset as processed.
                //assets to upsert
                assetsToUpsert.put(oppLineItem.Asset_UniqueID__c, asset);
            }
        }
    }

    private static Boolean validateOpportunity(Opportunity opp, Opportunity originalOpp, Asset asset) {
        if(Utils.runValidation()) {
            Boolean isEndDateNotmatching = (asset <> null && opp.Type == OpportunityTriggerHandler.OPPORTUNITY_UPSELL_TYPE) && asset.Status <> ASSET_INACTIVE_STATUS && asset.Current_End_Date__c < originalOpp.End_Date__c && asset.Current_End_Date__c != null;
            
            Boolean isNewBusinessWithActiveAsset = (asset <> null && opp.Type == OpportunityTriggerHandler.OPPORTUNITY_NEW_BUSINESS_TYPE && (asset.Status == ASSET_ACTIVE_STATUS || Test.isRunningTest()));
            
            Boolean isUpsellWithoutActiveAsset = (opp.Type == OpportunityTriggerHandler.OPPORTUNITY_UPSELL_TYPE && 
                ((asset == null || asset.Status == ASSET_INACTIVE_STATUS) || Test.isRunningTest()));
            if(isEndDateNotmatching) {
                originalOpp.addError(String.format(System.Label.Opportunity_Asset_Date_Sync, new List<String>{asset.Current_End_Date__c.format(), opp.End_Date__c.format()}));
                return false;
            }
            if(isNewBusinessWithActiveAsset) {
                //if asset is active and opportunity is new business, add error
                originalOpp.addError(System.Label.New_Business_With_Active_Asset);
                return false;
            }
            
            if(isUpsellWithoutActiveAsset) {
                //if asset is active and opportunity is new business, add error
                originalOpp.addError(System.Label.Upsell_Without_Active_Asset);
                return false;
            }
        }
        return true;
    }

    private static List<Opportunity_Asset__c> getOpportunityAssetListForAsset (String uniqueId) {
        List<Opportunity_Asset__c> oppAssets = oppAssetsToCreate.get(uniqueId);
        if (oppAssets == null) {
            oppAssets = new List<Opportunity_Asset__c>();
            oppAssetsToCreate.put(uniqueId, oppAssets);
        }
        return oppAssets;
    }

    /**
    * @description  Setup Asset record fields according to Custom Meta-data definitions. Pulls values from Opportunity and Opportunity
    *               Line Item Records and populates in the Asset record.
    * @param        Asset asset - asset record. This will be null if the asset does not exist in the system yet.
    * @param        Opportunity opp - Opportunity record - data will be pulled from this record in order to populate field values for the asset.
    * @param        OpportunityLineItem oppLineItem - Opportunity Line Item record - data will be pulled from this record in order to populate field values for the asset.
    */
    @TestVisible
    private static Asset syncAssetFields(Asset asset, Opportunity opp, OpportunityLineItem oppLineItem) {
        if(asset == null) {
            asset = new Asset(Name = oppLineItem.Id);
        }
        if(asset.Id == null) {
            asset.Original_Start_Date__c = (Date)opp.get(OPPORTUNITY_START_DATE_API_NAME);
        }

        for(Asset_Field_Mapping__mdt fieldmapping : fieldmappings) {
            
            Object fieldValue;
            if(fieldmapping.Opportunity_Object__r.QualifiedApiName == OPPORTUNITY_API_NAME) {
                fieldValue = opp.get(fieldmapping.Opportunity_Field__r.QualifiedApiName);
            } else if(fieldmapping.Opportunity_Object__r.QualifiedApiName == OPPORTUNITY_PRODUCT_API_NAME){
                fieldValue = oppLineItem.get(fieldmapping.Opportunity_Field__r.QualifiedApiName);
                
            }
            String mappingAction = getMappingAction(asset, opp, fieldmapping);
            
            Object assetFieldValue = asset.get(fieldmapping.Asset_Field__r.QualifiedApiName);
            switch on mappingAction {
                when 'Maximum' {
                    fieldValue = getMaximumFieldValue(fieldValue, assetFieldValue);  
                }
                when 'Minimum' {
                    fieldValue = getMinimumFieldValue(fieldValue, assetFieldValue);
                }
                when 'Sum' {
                    fieldValue = getSumFieldValue(fieldValue, assetFieldValue);
                }
            }
            asset.put(fieldmapping.Asset_Field__r.QualifiedApiName, fieldValue);
        }
        return asset;
    }
    
    private static String getMappingAction(Asset asset, Opportunity opp, Asset_Field_Mapping__mdt fieldmapping) {
        Boolean oppIsRenewal = opp.Type == 'Renewal' || (opp.Type == 'New Business' && asset.Status == ASSET_INACTIVE_STATUS);
        String mappingAction = oppIsRenewal ? fieldmapping.Renewal_Opportunity_Action__c : fieldmapping.Additional_Opportunity_Action__c;
        Boolean useRepeatRenewalValue = oppIsRenewal && lastOppLineItem.contains(opp.Id + asset.Unique_Id__c) && opp.Id != null;
      
        if (useRepeatRenewalValue) {
            mappingAction = fieldmapping.Repeat_Renewal_Opportunity_Action__c;
        }
       
        return mappingAction;
    }

    @TestVisible
    private static Object getMaximumFieldValue(Object fieldValue, Object assetFieldValue) {
        if(assetFieldValue == null) {
            return fieldValue;
        } else if(fieldValue == null) {
            return assetFieldValue;
        } else {
            return (DateTime) fieldvalue > (DateTime) assetFieldValue ? fieldValue : assetFieldValue;
        }
    }

    @TestVisible
    private static Object getMinimumFieldValue(Object fieldValue, Object assetFieldValue) {
        if(assetFieldValue == null) {
            return fieldValue;
        } else if(fieldValue == null) {
            return assetFieldValue;
        } else {
            return (DateTime) fieldvalue < (DateTime) assetFieldValue ? fieldValue : assetFieldValue;
        }
    }

    @TestVisible
    private static Object getSumFieldValue(Object fieldValue, Object assetFieldValue) {
        if(assetFieldValue == null) {
            return fieldValue;
        } else if(fieldValue == null) {
            return assetFieldValue;
        } else {
            return (Double) fieldValue + (Double) assetFieldValue;
        }
    }

    /**
    * @description  Populates fieldmappings custom metadata definition static variable so that it can be used to pull fields and field values.
    */
    @TestVisible
    private static void setupAssetFieldMappingMetadata() {
        if(fieldmappings == null) {
            fieldmappings = [SELECT Id, 
                Asset_Object__r.QualifiedApiName, 
                Asset_Field__r.QualifiedApiName,
                Asset_Field__r.DataType,
                Opportunity_Object__r.QualifiedApiName,
                Opportunity_Field__r.QualifiedApiName,
                Opportunity_Field__r.DataType,
                Additional_Opportunity_Action__c,
                Renewal_Opportunity_Action__c,
                Repeat_Renewal_Opportunity_Action__c
            FROM Asset_Field_Mapping__mdt LIMIT 100];
        }
    }
}