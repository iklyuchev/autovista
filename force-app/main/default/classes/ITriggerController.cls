/**
* @author Sapienti7
* @date 20/12/2018
* @description Defines the interface for trigger controller. Logic for the first time events are placed under the mainEntry 
*				method and the logic for the subsequent events raised on the same transaction (reentrant) are placed under 
*				the inProgressEntry method. The trigger event entries are placed under onBeforeInsert, onBeforeUpdate etc. methods
*/

public interface ITriggerController {

    /** 
	* @author Sapienti7
	* @date 20/12/2018
	* @description Called for the first time in the execution context.
	*/
	void mainEntry(String triggerObject, Boolean isBefore, 
                   Boolean isDelete, Boolean isAfter,
                   Boolean isInsert, Boolean isUpdate, 
                   Boolean isUndelete, Boolean isExecuting,
                   List<SObject> newlist, Map<Id, SObject> newmap,
                   List<SObject> oldlist, Map<Id, SObject> oldmap);
	
	/** 
	* @author Sapienti7
	* @date 20/12/2018
	* @description Called for the subsequent times in the same execution context.
	*/
	void inProgressEntry(String triggerObject, Boolean isBefore, 
                         Boolean isDelete, Boolean isAfter,
                         Boolean isInsert, Boolean isUpdate, 
                         Boolean isUndelete, Boolean isExecuting,
                         List<SObject> newlist, Map<Id, SObject> newmap,
                         List<SObject> oldlist, Map<Id, SObject> oldmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for before insert trigger event
	*/
    void onBeforeInsert(List<SObject> newlist);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for before update trigger event
	*/
    void onBeforeUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for before delete trigger event
	*/
    void onBeforeDelete(List<SObject> oldlist, Map<Id, SObject> oldmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for after inert trigger event
	*/
    void onAfterInsert(List<SObject> newlist, Map<Id, SObject> newmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for after update trigger event
	*/
    void onAfterUpdate(List<SObject> newlist, Map<Id, SObject> newmap, List<SObject> oldlist, Map<Id, SObject> oldmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for after delete trigger event
	*/
    void onAfterDelete(List<SObject> oldlist, Map<Id, SObject> oldmap);

	/**
	* @author Sapienti7
	* @date 20/12/2018
	* @description Used for after undelete trigger event
	*/
    void onAfterUndelete(List<SObject> newlist, Map<Id, SObject> newmap);
}