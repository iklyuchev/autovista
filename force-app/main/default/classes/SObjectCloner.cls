/**
* @author 		Sapienti7
* @date 		10/05/2019
* @description  Provides the ability to clone SObject records. Set default values of the cloned records according to
				Cloned_Object_Fields_Default_Value__mdt custom metadata records.
*/
public without sharing class SObjectCloner {
    private static Map<String, Map<String, Map<String, Cloned_Object_Fields_Default_Value__mdt>>> customMetadataTypesByFieldByObjectByCloningProcess;	// cp => object => field => cmt
	private static Map<String, Cloned_Object_Fields_Default_Value__mdt> currentCustomMetadataTypesByField;
	private static String currentCloningProcess;
	private static String currentObjectName;
	@Testvisible
	private static Boolean simulateDMLException = false;

	/*
	*	@description	Given a record of any SObject and a string to filter by cloning process (it is a field on the custom metadata type), this method clones the record to a new one, 
	*					copying the same values from the source record except the fields set up in the custom metadata type, where default values are set up for those fields.
	*					The method returns the cloned record and optionally stores it in the database if doInsert = true.
	*					The cloning process parameter is used to define more than one cloning process (sets of default values) for the same object.
	*					IMPORTANT: The standard SObject's clone() method only clones the fields that are populated on the source record; therefore, if a deep clone is needed on all the fields, 
	*					the source record must provide all the fields.
	*/
	public static SObject cloneRecord(SObject record, String cloningProcess, Boolean doInsert, String objectType){
		Map<String, Cloned_Object_Fields_Default_Value__mdt> customMetadataTypesByField = getCustomMetadataTypesByField(cloningProcess, objectType);

		SObject clonedRecord = record.clone(false, true, false, false);

		try{
			for (String fieldName : customMetadataTypesByField.keyset()){
				Cloned_Object_Fields_Default_Value__mdt cmType = customMetadataTypesByField.get(fieldName);
				setDefaultValue(record, clonedRecord, fieldName, cmType);
			}

			if(simulateDMLException) {
				throw new DmlException();
			}
			if (doInsert){
	            insert clonedRecord;
	        }
		}
		catch (Exception e){
			record.addError(e);
		}
		
		return clonedRecord;
	}


	/*
	*	@description	Helper method that sets the field 'fieldName' on the record 'clonedRecord' to its default 
	*					value based on the given custom metadata type 'cmType'. If it is a dynamic default value, 
	*					it will be retrieved from the original record 'record'.
	*/
	private static void setDefaultValue(SObject record, SObject clonedRecord, String fieldName, Cloned_Object_Fields_Default_Value__mdt cmType){
		if (String.isBlank(cmType.Dynamic_Default_Value__c)){
	    	if (cmType.Cloned_Field__r.DataType.containsIgnoreCase('number') || cmType.Cloned_Field__r.DataType.containsIgnoreCase('percent')){
	    		clonedRecord.put(fieldName, cmType.Default_Value_NUMBER__c);
	    	}
	    	else if (cmType.Cloned_Field__r.DataType.containsIgnoreCase('checkbox')){
	    		clonedRecord.put(fieldName, cmType.Default_Value_CHECKBOX__c);
	    	}
	    	else if (cmType.Cloned_Field__r.DataType.equalsIgnoreCase('date')){
	    		clonedRecord.put(fieldName, cmType.Default_Value_DATE__c);
	    	}
	    	else if (cmType.Cloned_Field__r.DataType.equalsIgnoreCase('date/time')){
	    		clonedRecord.put(fieldName, cmType.Default_Value_DATE_TIME__c);
	    	}
	    	else{
	    		clonedRecord.put(fieldName, cmType.Default_Value_TEXT__c);
	    	}
	    }
	    else {
	    	clonedRecord.put(fieldName, record.get(cmType.Dynamic_Default_Value__r.QualifiedApiName));
	    }
	}

	/*
	*	@description	Helper method that loads the static map customMetadataTypesByFieldByObjectByCloningProcess 
	*					from the database and returns the inner map related to the given cloning process and object.
	*/
	@TestVisible
	private static Map<String, Cloned_Object_Fields_Default_Value__mdt> getCustomMetadataTypesByField(String cloningProcess, String objectName){
		if (currentObjectName == objectName && currentCloningProcess == cloningProcess){
			return currentCustomMetadataTypesByField;
		}

		currentObjectName = objectName;
		currentCloningProcess = cloningProcess;

		if (customMetadataTypesByFieldByObjectByCloningProcess == null){
			customMetadataTypesByFieldByObjectByCloningProcess = new Map<String, Map<String, Map<String, Cloned_Object_Fields_Default_Value__mdt>>>();
		}
		
		if (customMetadataTypesByFieldByObjectByCloningProcess.get(cloningProcess) == null){
			Map<String, Map<String, Cloned_Object_Fields_Default_Value__mdt>> customMetadataTypesByFieldByObject = new Map<String, Map<String, Cloned_Object_Fields_Default_Value__mdt>>();
			customMetadataTypesByFieldByObjectByCloningProcess.put(cloningProcess, customMetadataTypesByFieldByObject);
		}
		
		if (customMetadataTypesByFieldByObjectByCloningProcess.get(cloningProcess).get(objectName) == null){
			List<Cloned_Object_Fields_Default_Value__mdt> cmtDefaultValues = queryCustomMetadataTypes(cloningProcess, objectName);

			Map<String, Cloned_Object_Fields_Default_Value__mdt> customMetadataTypesByField = new Map<String, Cloned_Object_Fields_Default_Value__mdt>();
			for (Cloned_Object_Fields_Default_Value__mdt cmtDefaultValue : cmtDefaultValues){
				customMetadataTypesByField.put(cmtDefaultValue.Cloned_Field__r.QualifiedApiName.toLowerCase(), cmtDefaultValue);
			}

			customMetadataTypesByFieldByObjectByCloningProcess.get(cloningProcess).put(objectName, customMetadataTypesByField);
			currentCustomMetadataTypesByField = customMetadataTypesByField;
		} else {
			currentCustomMetadataTypesByField = customMetadataTypesByFieldByObjectByCloningProcess.get(cloningProcess).get(objectName);
		}

		return currentCustomMetadataTypesByField;
	}

	@Testvisible
	private static List<Cloned_Object_Fields_Default_Value__mdt> queryCustomMetadataTypes(String cloningProcess, String objectName){
		return [
			SELECT Cloned_Object__c, 
				Cloned_Object__r.QualifiedApiName, 
				Cloned_Field__c, 
				Cloned_Field__r.QualifiedApiName, 
				Cloned_Field__r.DataType, 
				Cloning_Process__c,
				Default_Value_TEXT__c, 
				Default_Value_NUMBER__c, 
				Default_Value_CHECKBOX__c, 
				Default_Value_DATE__c, 
				Default_Value_DATE_TIME__c,
				Dynamic_Default_Value__c, 
				QualifiedApiName, 
				Dynamic_Default_Value__r.QualifiedApiName
			FROM Cloned_Object_Fields_Default_Value__mdt
			WHERE Cloned_Object__r.QualifiedApiName = :objectName AND Cloning_Process__c = :cloningProcess
		];
	}
}