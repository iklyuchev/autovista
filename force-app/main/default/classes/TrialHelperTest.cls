/**
* @author       Sapienti7
* @date         10/05/2019
* @description  Unit test for TrialHelper. Tests Automatic trial Opportunity creation.
*/
@isTest
private class TrialHelperTest {

    @testSetup
    private static void setupData() {
        TestDataFactory.insertCustomSettings(false, false, true);
        
        Account account = TestDataFactory.createAccount(true, 'Test');
        Opportunity opp = TestDataFactory.createOpportunity(true, 'New Test', account.Id, System.Label.OpportunityFirstContactStage, System.today().addMonths(1), 'United Kingdom', 12, false);
        Opportunity trialOpp = TestDataFactory.createOpportunity(false, 'Trial Test', account.Id, System.Label.OpportunityFirstContactStage, System.today().addMonths(1), 'United Kingdom', 3, true);
        trialOpp.Parent_Opportunity__c = opp.Id;
        insert trialOpp;

        Contact contact = TestDataFactory.createContact(true, account.id, 'Test', 'Tester', 'test@test.com');
        TestDataFactory.createOpportunityContactRole(true, contact.id, opp.Id, 'Business User', true);

        opp.StageName = System.Label.OpportunityTrialStage;
        update opp;

        List<Product2> products = TestDataFactory.createProductsWithPriceBook(2, 25.00, true);
        TestDataFactory.createOpportunityLineItems(true, new List<Opportunity> {opp}, products, 30.00, 2);
        TestDataFactory.createOpportunityLineItems(true, new List<Opportunity> {trialOpp}, new List<Product2>{products[0]}, 30.00, 2);

        List<Asset> assets = TestDataFactory.createAssets(true, 2, account.Id, products[0].Id, System.today(), 3, true);

    }

    @isTest
    private static void testGetOpportunityTrialData() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'New Test' LIMIT 1];
    
        Test.startTest();

        String prodStr = TrialHelper.getOpportunityTrialData(opp.Id);

        Test.stopTest();

        Opportunity opportunity = (Opportunity)JSON.deserialize(prodStr, Opportunity.class);
        System.assertNotEquals(null, opportunity, 'Opportunity is not retrieved');
        System.assertEquals(1, opportunity.OpportunityLineItems.size(), 'Line Items are not retrieved');
    }

    @isTest
    private static void testGetOpportunityTrialDataWithNullId() {
       
        Test.startTest();

        String prodStr = TrialHelper.getOpportunityTrialData(null);

        Test.stopTest();

        System.assertEquals(null, prodStr, 'Method must return null if no opp id provided.');
    }


    @isTest
    private static void testCreateOpportunityTrialsWithExistingTrialOpp() {
        Opportunity opp = [SELECT Id, (SELECT Id FROM OpportunityLineItems LIMIT 1) FROM Opportunity WHERE Name = 'New Test'  LIMIT 1];
        Opportunity trialOpp = [SELECT Id FROM Opportunity WHERE RecordTypeId =: TrialHelper.trialRecordTypeId LIMIT 1];
        List<Opportunity> allcurrentOpportunities = [SELECT Id FROM Opportunity];
        
        List<Object> freeTrialsList = new List<Object>{opp.OpportunityLineItems[0].Id};
        Map<String, Object> selectedItemMap = new Map<String, Object>{
            'duration' => 3,
            'items' => freeTrialsList
        };
        Test.startTest();

        TrialHelper.createOpportunityTrials(opp.Id, JSON.serialize(selectedItemMap));

        Test.stopTest();

        trialOpp = [SELECT Id, (SELECT Id FROM OpportunityLineItems) FROM Opportunity WHERE Id = :trialOpp.Id LIMIT 1];
        System.assertNotEquals(null, trialOpp, 'Trial Opporutnity should be avialble');
        System.assertEquals(2, trialOpp.OpportunityLineItems.size(), 'Opportunity Line Items are not copied to the trial opp');
    }

    @isTest
    private static void testCreateOpportunityTrialsWithoutExistingTrialOpp() {
        Opportunity opp = [SELECT Id, (SELECT Id FROM OpportunityLineItems LIMIT 1) FROM Opportunity WHERE Name = 'New Test'  LIMIT 1];
        Opportunity trialOpp = [SELECT Id FROM Opportunity WHERE RecordTypeId =: TrialHelper.trialRecordTypeId LIMIT 1];
        delete trialOpp;

        List<Opportunity> allcurrentOpportunities = [SELECT Id FROM Opportunity];
        List<Object> freeTrialsList = new List<Object>{opp.OpportunityLineItems[0].Id};
        Map<String, Object> selectedItemMap = new Map<String, Object>{
            'duration' => 3,
            'items' => freeTrialsList
        };

        Test.startTest();

        TrialHelper.createOpportunityTrials(opp.Id, JSON.serialize(selectedItemMap));

        Test.stopTest();

        trialOpp = [SELECT Id, (SELECT Id FROM OpportunityLineItems) FROM Opportunity WHERE Parent_Opportunity__c = :opp.Id AND RecordTypeId =: TrialHelper.trialRecordTypeId LIMIT 1];
        System.assertNotEquals(null, trialOpp, 'Trial Opporutnity should be avialble');
        System.assertEquals(1, trialOpp.OpportunityLineItems.size(), 'Opportunity Line Items are not copied to the trial opp');
    }

    @isTest
    private static void testCreateOpportunityTrialsAsReadOnlyUser() {

        User readOnlyUser = TestDataFactory.createUser(true, 'Read Only', 'test@test.com', 'avunittestxzy@avunittestxzy.com');
        Opportunity opp = [SELECT Id, (SELECT Id FROM OpportunityLineItems LIMIT 1) FROM Opportunity WHERE Name = 'New Test' LIMIT 1];
        List<Opportunity> allcurrentOpportunities = [SELECT Id FROM Opportunity];

        List<Object> freeTrialsList = new List<Object>{opp.OpportunityLineItems[0].Id};

        Map<String, Object> selectedItemMap = new Map<String, Object>{
            'duration' => 3,
            'items' => freeTrialsList
        };
        Test.startTest();
        try {
            System.runAs(readOnlyUser) {
                TrialHelper.createOpportunityTrials(opp.Id, JSON.serialize(selectedItemMap));
                System.assert(false, 'Error is exception from the above line.');
            }
        } catch (Exception ex) {
            System.assertNotEquals(null, ex);
        }

        Test.stopTest();

        List<Opportunity> trialOpp = [SELECT Id FROM Opportunity WHERE Id NOT IN: allcurrentOpportunities LIMIT 1];
        System.assertEquals(0, trialOpp.size(), 'Trial Opporutnity must not be created');
    }

    @isTest
    private static void testGetCurrentlyTrialedProducts() {
        List<Asset> assets = [SELECT Id FROM Asset];
        for(Asset a : assets) {
            a.Is_Free_Trial__c = true;
        }
        update assets;

        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'New Test'  LIMIT 1];
        Set<Id> productIds = TrialHelper.getCurrentlyTrialedProducts(opp.Id);

        System.assertEquals(1, productIds.size(), 'One products should be currently trialled.');
    }

    @isTest
    private static void testCreateOpportunityTrialsOnError() {
        Opportunity opp = [SELECT Id, (SELECT Id FROM OpportunityLineItems LIMIT 1) FROM Opportunity WHERE Name = 'New Test' LIMIT 1];
        List<Opportunity> allcurrentOpportunities = [SELECT Id FROM Opportunity];
        
        List<Object> freeTrials = new List<Object>();
        
        try {
            Test.startTest();
            //parse invalid JSON string
            TrialHelper.createOpportunityTrials(opp.Id, JSON.serialize(freeTrials));
            Test.stopTest();

            System.assert(false, 'Error is exception from the above line.');
        } catch (Exception ex) {
            System.assertNotEquals(null, ex);
        }
        
        List<Opportunity> trialOpp = [SELECT Id FROM Opportunity WHERE Id NOT IN: allcurrentOpportunities LIMIT 1];
        System.assertEquals(0, trialOpp.size(), 'Trial Opporutnity must not be created');
    }

    @isTest
    private static void testCreateTrialOpportunity() {
        Opportunity opp = [SELECT Id, AccountId, Sales_Country__c, Country__c, Company__c, Pricebook2Id, CurrencyIsoCode FROM Opportunity WHERE Name = 'New Test' LIMIT 1];
        
        Test.startTest();

        Opportunity trial = TrialHelper.createNewTrialOpportunity(opp);

        Test.stopTest();

        System.assertNotEquals(null, trial, 'Trial Opportunity must be created');
        System.assertEquals(TrialHelper.trialRecordTypeId, trial.RecordTypeId, 'Record type should be set to Trial');
        System.assertEquals(opp.AccountId, trial.AccountId, 'Account Id should be set to parent Opp account id');
        System.assertEquals(opp.Id, trial.Parent_Opportunity__c, 'Parent opp must be set');
        System.assertEquals(TrialHelper.OFFERED_STAGE, trial.StageName, 'Opp stage should be set to '+TrialHelper.OFFERED_STAGE);
    }

   @isTest
    private static void testCreateOpportunityLineItem() {
        Opportunity opp = [SELECT Id, (SELECT Id, Product2Id, OpportunityId, Revenue_Type__c, Quantity FROM OpportunityLineItems LIMIT 1) FROM Opportunity WHERE Name = 'New Test'  LIMIT 1];
        
        Test.startTest();

        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        for(OpportunityLineItem oppLine : opp.OpportunityLineItems) {
            OpportunityLineItem newOppLine = TrialHelper.createOpportunityLineItem(oppLine.Product2Id, oppLine.OpportunityId, null, oppLine.Revenue_Type__c, oppLine.Quantity, 0.0, true);
            oppLineItems.add(newOppLine); 
        }

        Test.stopTest();

        System.assertEquals(1, oppLineItems.size(), 'Opp Line item must be created');
        System.assertEquals(opp.OpportunityLineItems[0].Product2Id, oppLineItems[0].Product2Id, 'Product Id is invalid');
        System.assertEquals(opp.OpportunityLineItems[0].OpportunityId, oppLineItems[0].OpportunityId, 'Opportunity Id is invalid');
        System.assertEquals(true, oppLineItems[0].Is_Free_Trial__c, 'Free trial flag should be set.'); 
    }
}