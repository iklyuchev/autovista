/**
* @author       Sapienti7
* @date         10/05/2019
* @description  TriggerHelper for Opportunity object.
*               Functionality: 1. Generate Renewal new Opportunities for order won Opportunities.
*/
public without sharing class OpportunityTriggerHelper {
    public static Map<Id,Opportunity> renewalOpportunities = new Map<Id,Opportunity>();
    
    private static final String CLONING_PROCESS = System.Label.OpportunityCloningProcess;
    private static final String PRIMARY_CONTACT_REQUIRED = System.Label.PrimaryContactRequired;
    private static final string OPPORTUNITY_RENEWAL_TYPE = System.Label.OpportunitySalesTypeRenewal;
    private static final String STANDARD = 'standard';
    private static final string REVENUE_TYPE_SUBSCRIPTION = System.Label.RevenueTypeSubscription;
    private static final String CLOSED_WON_STAGE = System.Label.OpportunityWonStage;
    private static final String CLOSED_LOST_STAGE = System.Label.OpportunityLostStage;
    @TestVisible
    private static List<Opportunity_Approvers__mdt> approversMetadata;

    /**
    * @description  Main helper method for generating renwal opportunities. This method calls other helper method separately to
    *               create Opportunities, OpporutnityLineItems, ContactRoles and OpportunityAssets.
    * @param        List<Opportunity> opportunities - List of Order Won opportunities, for each of these records, new opportunity will be created.
    */
    public static void createNewRenewalOpportunities(List<Opportunity> opportunities) {

        if(opportunities != null && opportunities.size() > 0) {

            renewalOpportunities = generateOpportunities(opportunities);

            generateOpportunityContactRoles();
            generateOpportunityProducts();
        }
    }

    /**
    * @description  When an Opportunity with type of "Up Sell" is closed won, related Renewal Opportunity 
    *               Line Item Quantities are incremented according to this Opportunity Line Item Quantities.
    *               Assumptions: If new Product is sold, to an existing client, it will NOT be sold as an Up Sell.
    * @param        List<Opportunity> upsellOpportunities - Opportunities with updated type = 'Up Sell'
    */
    public static void updateRenewalOpportunities(List<Opportunity> upsellOpportunities) {
        Log.push('OpportunityTriggerHelper.updateRenewalOpportunities');
        Map<String, OpportunityLineItem> upsellOppLineItemMapping = new Map<String, OpportunityLineItem>();
        //Get all upsell opportunity line items
        List<OpportunityLineItem> upsellOppLines = [SELECT Asset_UniqueID__c, Quantity FROM OpportunityLineItem WHERE OpportunityId = :upsellOpportunities];
        for(OpportunityLineItem upsellOppLine : upsellOppLines) {
            upsellOppLineItemMapping.put(upsellOppLine.Asset_UniqueID__c, upsellOppLine);
        }

        //Get related renewal line items
        Map<String, OpportunityLineItem> renewalOppLineMapping = new Map<String,OpportunityLineItem>();
        List<OpportunityLineItem> renewalOppLines = [SELECT Asset_UniqueID__c, UnitPrice, Quantity FROM OpportunityLineItem 
            WHERE Asset_UniqueID__c =: upsellOppLineItemMapping.keySet() 
            AND Opportunity.Type = :OPPORTUNITY_RENEWAL_TYPE
            AND Opportunity.StageName <> :CLOSED_WON_STAGE AND Opportunity.StageName <> :CLOSED_LOST_STAGE];
        List<OpportunityLineItem> renewalOppLinesToUpdate = new List<OpportunityLineItem>();

        for(OpportunityLineItem renewalOppLine : renewalOppLines) {
            if(upsellOppLineItemMapping.containsKey(renewalOppLine.Asset_UniqueID__c)) {

                OpportunityLineItem upsellLineItem = upsellOppLineItemMapping.get(renewalOppLine.Asset_UniqueID__c);
                renewalOppLine.Quantity = renewalOppLine.Quantity + upsellLineItem.Quantity;
                renewalOppLinesToUpdate.add(renewalOppLine);
            }
        }

        if(renewalOppLinesToUpdate.size() > 0) {
            List<Database.SaveResult> saveResults = Database.update(renewalOppLinesToUpdate, false);
            Utils.logSaveResults(saveResults);
        }
    }

    public static void assignApprovers(List<Opportunity> opportunities, Set<String> countries) {
        if(opportunities != null && opportunities.size() > 0 && countries != null && countries.size() > 0){
            
            Map<String, Opportunity_Approvers__mdt> approvers = getOpportunityApprovers(countries);
            for(Opportunity opportunity : opportunities) {
                if(approvers.containsKey(opportunity.Sales_Country__c)) {
                    Opportunity_Approvers__mdt approver = approvers.get(opportunity.Sales_Country__c);
                    opportunity.Country_Director_Approver__c = approver.Country_Director_Approver__c;
                    opportunity.Regional_Director_Approver__c = approver.Regional_Director_Approver__c;
                } else {
                    opportunity.Country_Director_Approver__c = null;
                    opportunity.Regional_Director_Approver__c = null;
                }
            }
        }
    }

    @TestVisible
    private static Map<String, Opportunity_Approvers__mdt> getOpportunityApprovers(Set<String> countries) {
        Map<String, Opportunity_Approvers__mdt> approversByCountry = new Map<String, Opportunity_Approvers__mdt>();
        if(approversMetadata == null) {
            approversMetadata = [SELECT Id, Country__c, Country_Director_Approver__c, Regional_Director_Approver__c FROM Opportunity_Approvers__mdt WHERE Country__c IN :countries];
        }
        if(approversMetadata != null && approversMetadata.size() > 0) {
            for(Opportunity_Approvers__mdt approver :  approversMetadata) {
                approversByCountry.put(approver.Country__c, approver);
            }
        }
        return approversByCountry;
    }

    /**
    * @description  This method queries for active, Standard Pricebook and any Pricebooks belong to the countries passed in.
    *               Then maps Country to Pricebook Id.
    */
    /*private static Map<String, Id> getPricebooksByCountry(Set<String> countries) {
        List<PriceBook2> priceBooks = [SELECT Id, IsStandard, Country__c FROM PriceBook2 WHERE IsActive = true AND (Country__c IN :countries OR IsStandard = true)];
        Map<String, Id> priceBookMapping = new Map<String,Id>();

        for(PriceBook2 pb : priceBooks) {
            if(pb.IsStandard) {
                priceBookMapping.put(STANDARD, pb.Id);
            } else {
                priceBookMapping.put(pb.Country__c, pb.Id);
            }
        }
        return priceBookMapping;
    }*/
    
    public static void assignPricebooks (List<Opportunity> opportunities, Set<String> countries) {
        List<PriceBook2> priceBooks = [SELECT Id, Country__c FROM PriceBook2 
            WHERE Country__c IN :countries AND Start_Date__c <= today AND End_Date__c >= today AND IsActive = true AND IsStandard = false];
        
        Map<String, Id> priceBookMapping = new Map<String,Id>();
        for(PriceBook2 pb : priceBooks) {
            priceBookMapping.put(pb.Country__c, pb.Id);
        }
        
        for(Opportunity o: opportunities)
        {
            if(priceBookMapping.containsKey(o.Sales_Country__c))
            {
                o.PriceBook2Id = priceBookMapping.get(o.Sales_Country__c);
            }
        }
    }

    /**
    * @description  This method create new instances of Opportunities and copy values from old opportunity to new.
    *               Also sets the Parent_Opportunity__c of the newly generated Opportunity.
    * @param        List<Opportunity> opportunities - List of Order Won opportunities, for each of these records, new opportunity will be created.
    */
    @TestVisible
    private static Map<Id,Opportunity> generateOpportunities(List<Opportunity> opportunities) {

        Map<Id,Opportunity> newOpportunities = new  Map<Id,Opportunity>();

        for(Opportunity opportunity : opportunities) {
            if(opportunity.Number_of_Renewal_Products__c > 0) {
                Opportunity newOpportunity = (Opportunity)SObjectCloner.cloneRecord(opportunity, CLONING_PROCESS, false, Schema.SObjectType.Opportunity.getName());
                newOpportunity.Parent_Opportunity__c = opportunity.Id;
                newOpportunities.put(opportunity.Id, newOpportunity);
            }
            
        }

        List<Database.SaveResult> saveResults = Database.insert(newOpportunities.values(), false);
        Utils.logSaveResults(saveResults);
        
        return newOpportunities;
    }

    /**
    * @description  Clones OpportunityContactRole records related to each regenerated Opportunity record and inserts to the database.
    */
    @TestVisible
    private static void generateOpportunityContactRoles() {
        
        List<OpportunityContactRole> oppContRoles = [SELECT OpportunityId,ContactId,IsPrimary,Role FROM OpportunityContactRole WHERE OpportunityId = :renewalOpportunities.keySet()];
        List<OpportunityContactRole> newOppContRoles = new List<OpportunityContactRole>();

        for(OpportunityContactRole oppContRole : oppContRoles) {
            OpportunityContactRole opc = (OpportunityContactRole)SObjectCloner.cloneRecord(oppContRole, CLONING_PROCESS, false, Schema.SObjectType.OpportunityContactRole.getName());
            opc.OpportunityId = renewalOpportunities.get(oppContRole.OpportunityId).Id;
            newOppContRoles.add(opc);
        }

        List<Database.SaveResult> saveResults = Database.insert(newOppContRoles, false);
        Utils.logSaveResults(saveResults);
    }

    /**
    * @description  Clones OpporutnityLineItems related to each regenerated Opportunity record and inserts to the database.
    */
    @TestVisible
    private static void generateOpportunityProducts() {

        Set<Id> opportunityIds = renewalOpportunities.keySet();
        Set<String> oppLineFields = Utils.getAllFields('OpportunityLineItem');
        //clonesourceid field is removed from the set in order to avoid query exception as this field is not available for SOQL query.
        oppLineFields.remove('clonesourceid');
        String oppLineQuery = 'SELECT '+String.join(new List<String>(oppLineFields), ',') + ' FROM OpportunityLineItem WHERE OpportunityId = :opportunityIds AND Revenue_Type__c = :REVENUE_TYPE_SUBSCRIPTION';
        List<OpportunityLineItem> oppLineItems = Database.query(oppLineQuery);
        List<OpportunityLineItem> newOppLineItems = new List<OpportunityLineItem>();
       
        for(OpportunityLineItem oppLineItem : oppLineItems) {
            OpportunityLineItem opl = (OpportunityLineItem)SObjectCloner.cloneRecord(oppLineItem, CLONING_PROCESS, false, Schema.SObjectType.OpportunityLineItem.getName());
            opl.OpportunityId = renewalOpportunities.get(oppLineItem.OpportunityId).Id;
            newOppLineItems.add(opl);

        }
        List<Database.SaveResult> saveResults = Database.insert(newOppLineItems, false);
        Utils.logSaveResults(saveResults);
    }

    /**
    * @description  Validate primary contact is available.
    */
    public static void validatePrimaryContact(List<Opportunity> opportunities) {
        if(Utils.runValidation()) {
            Map<Id,Opportunity> oppContactRoles = new Map<Id,Opportunity>([SELECT Id, (SELECT Id FROM OpportunityContactRoles WHERE IsPrimary = true LIMIT 1) 
                FROM Opportunity WHERE Id IN :opportunities]);

            if(oppContactRoles.size() > 0) {
                for(Opportunity opp : opportunities) {
                    if(oppContactRoles.containsKey(opp.Id) && oppContactRoles.get(opp.Id).OpportunityContactRoles.size() == 0) {
                        opp.addError(PRIMARY_CONTACT_REQUIRED);
                    }
                }
            }
        }
    }

    /**
    * @description  Create Trial Opportunities and Line Items according to Parent Opportunities where Product Code is populated.
    *               Relies on PricebookId being populated from before insert trigger.
    */
    public static void createTrialOpportunities(Map<Id, Opportunity> parentOpportunities, Set<String> allProductCodes) {
        
        
        
        Map<String,PricebookEntry> productMapping = getPriceEntryMapping(allProductCodes);

        if(productMapping.size() > 0) {

            Map<Id,Opportunity> trialOpportunities = new Map<Id,Opportunity>();
            Map<Id,OpportunityLineItem> trialOpportunityLineItems = new Map<Id,OpportunityLineItem>();
            Map<Id,List<OpportunityContactRole>> trialContactRolesMap = new Map<Id,List<OpportunityContactRole>>();
            List<OpportunityLineItem> parentOppLineItems = new List<OpportunityLineItem>();

            if(parentOpportunities.size() > 0) {

                for(Opportunity o : parentOpportunities.values()) 
                {
                    String priceEntryIdentifier = o.PriceBook2Id + o.CurrencyIsoCode + o.Product_Code__c;
                    //check product is available
                    if(productMapping.containsKey(priceEntryIdentifier)) {
                        
                        Opportunity trialOpp = TrialHelper.createNewTrialOpportunity(o);

                        //Create Trial Opportunity line item
                        Id priceEntry = productMapping.get(priceEntryIdentifier).Id;
                        Double price = productMapping.get(priceEntryIdentifier).UnitPrice;
                       
                        OpportunityLineItem trialOppLine = TrialHelper.createOpportunityLineItem(null, null, priceEntry, o.Revenue_Type__c, 1, 0, true);
                        
                        //Create line item for parent opportunity
                        OpportunityLineItem parentOppLine = TrialHelper.createOpportunityLineItem(null, o.Id, priceEntry, o.Revenue_Type__c, 1, price, true);

                        trialOpportunities.put(o.Id, trialOpp);
                        trialOpportunityLineItems.put(o.Id, trialOppLine);
                        parentOppLineItems.add(parentOppLine);
                    }
                }
                //insert Trial Opps and Line items
                createTrialsAndLineItems(trialOpportunities, trialOpportunityLineItems, parentOppLineItems);
            }
        }
    }

    /**
    * @description  Provides the Product Id mapped by Product Code for all the product codes passed into this method.
    */
    private static Map<String, Id> getProductMapping(Set<String> allProductCodes) {
        Map<String, Id> productCodeMapping = new Map<String, Id>();
        if(allProductCodes.size() > 0) {
            List<Product2> products = [SELECT Id,ProductCode FROM Product2 WHERE ProductCode =: allProductCodes AND Is_Free_Trial__c = true];
            for(Product2 product : products) {
                productCodeMapping.put(product.ProductCode, product.Id);
            }
        }
       return productCodeMapping;
    }
    
    /**
    * @description  Provides the Product Id mapped by Product Code for all the product codes passed into this method.
    */
    private static Map<String, PricebookEntry> getPriceEntryMapping(Set<String> allProductCodes) {
        Map<String, PricebookEntry> priceEntryMapping = new Map<String, PricebookEntry>();
        if(allProductCodes.size() > 0) {
            List<PricebookEntry> products = [SELECT Id, Pricebook2Id, ProductCode, CurrencyIsoCode, UnitPrice FROM PricebookEntry 
                WHERE ProductCode IN :allProductCodes AND Product2.Is_Free_Trial__c = true];
            for(PricebookEntry p : products) {
                priceEntryMapping.put(p.Pricebook2Id + p.CurrencyIsoCode + p.ProductCode, p);
            }
        }
       return priceEntryMapping;
    }

    /**
    * @description  Inserts Trial Opportunities and populates trial opp id on line items before inserting opp line items.
    */
    private static void createTrialsAndLineItems(Map<Id,Opportunity> trialOpportunities, Map<Id,OpportunityLineItem> trialOpportunityLineItems, List<OpportunityLineItem> parentOppLineItems) {
        if(trialOpportunities.size() > 0 && trialOpportunityLineItems.size() > 0) {

            List<Database.SaveResult> oppSaveResults = Database.insert(trialOpportunities.values(), false);
            Utils.logSaveResults(oppSaveResults);
            
            List<OpportunityLineItem> lineItemsToInsert = new List<OpportunityLineItem>();
            //assign trail opportunity id on line items
            for(Id parentId : trialOpportunityLineItems.keySet()) {
                trialOpportunityLineItems.get(parentId).OpportunityId = trialOpportunities.get(parentId).Id;
                lineItemsToInsert.add(trialOpportunityLineItems.get(parentId));
            }
            lineItemsToInsert.addAll(parentOppLineItems);
            
            List<Database.SaveResult> oppItemsaveResults =  Database.insert(lineItemsToInsert, false);
            Utils.logSaveResults(oppItemsaveResults);
        }
    }

}