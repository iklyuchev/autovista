/**
* @author 		Sapienti7
* @date 		10/05/2019
* @description  Unit test for SObjectCloner. Tests cloning of SObject instances with setup of default values according to
*				Cloned_Object_Fields_Default_Value__mdt metadata.
*/
@isTest
private class SObjectClonerTest {
    
	@testSetup
	private static void setupData() {
		TestDataFactory.insertCustomSettings(false, false, true);
		Account account = TestDataFactory.createAccount(true, 'Test');
		TestDataFactory.createOpportunities(true, 200, account.Id);
	}

	@isTest
	private static void testCloneRecord(){
		List<Opportunity> opp = [SELECT Id, AccountId, Name, Stagename, CloseDate, Next_Start_Date__c, End_Date__c  FROM Opportunity ORDER BY Name LIMIT 1];

		Test.startTest();

		Opportunity clonedOpp = (Opportunity)SObjectCloner.cloneRecord(opp[0], System.Label.OpportunityCloningProcess, true, Schema.SObjectType.Opportunity.getName());

		Test.stopTest();

		//validate opportunity record is cloned
		List<Opportunity> cloneOppRecord = [SELECT Id, Name FROM Opportunity WHERE Id = :clonedOpp.Id];
		System.assertEquals(1, cloneOppRecord.size(), 'Invalid number of cloned records');

		//validate default values are populated
		String oppObjName = Schema.SObjectType.Opportunity.getName();
		String oppStageNameField =  Schema.Opportunity.StageName.getDescribe().getName();
		Cloned_Object_Fields_Default_Value__mdt defaultValueMeta = TestDataFactory.getDefaultMetadataField(oppObjName, oppStageNameField);
		
		if(defaultValueMeta != null) {
			System.assertEquals(defaultValueMeta.Default_Value_TEXT__c, clonedOpp.StageName, 'Invalid default value for stage.');
		}

		//validate dynamic field values are populated
		String oppCloseDateField =  Schema.Opportunity.CloseDate.getDescribe().getName();
		Cloned_Object_Fields_Default_Value__mdt defaultDynamicMeta = TestDataFactory.getDefaultMetadataField(oppObjName, oppCloseDateField);
		if(defaultDynamicMeta != null) {
			String dynamicField = defaultDynamicMeta.Dynamic_Default_Value__r.QualifiedApiName;
			Opportunity clonedDynamicOpp = Database.query('SELECT Id, '+dynamicField+' FROM '+oppObjName+' WHERE Id = \''+opp[0].Id+'\'');
			System.assertEquals(clonedDynamicOpp.get(dynamicField), clonedOpp.get(oppCloseDateField), 'Invalid dynamic default value for close date.');
		}
	}

	@isTest
	private static void testCloneRecordBulk(){
		List<Opportunity> opps = [SELECT Id, AccountId, Name, Stagename, CloseDate, Next_Start_Date__c, End_Date__c  FROM Opportunity ORDER BY Name LIMIT 200];

		Test.startTest();

		List<Opportunity> newOpps = new List<Opportunity>();
		for(Opportunity opp : opps) {
			Opportunity clonedOpp = (Opportunity)SObjectCloner.cloneRecord(opp, System.Label.OpportunityCloningProcess, false, Schema.SObjectType.Opportunity.getName());
			newOpps.add(clonedOpp);
		}

		Test.stopTest();

		//validate 200 records are cloned
		System.assertEquals(200, newOpps.size(), 'Invalid number of Cloned Opportunities');

		for(Opportunity opp : newOpps) {
			System.assertEquals(null, opp.Id, 'Opportunity Id must be null');
		}
	}

	@isTest
	private static void testCloneRecordError(){
		List<Opportunity> opp = [SELECT Id, AccountId, Name, Stagename, CloseDate, Next_Start_Date__c, End_Date__c  FROM Opportunity ORDER BY Name LIMIT 1];

		Test.startTest();
		
		SObjectCloner.simulateDMLException = true;
		Opportunity clonedOpp = (Opportunity)SObjectCloner.cloneRecord(opp[0], System.Label.OpportunityCloningProcess, true, Schema.SObjectType.Opportunity.getName());
		
		Test.stopTest();

		System.assertEquals(null, clonedOpp.Id, 'Cloned record should not be inserted.');
	}
}