#!/bin/bash

# Don't exit on error
set +e

# Set code coverage requirement for CI build, 90 by default
if [ "$bamboo_codeCoverageRequirement" ]
then
    export SFDX_CODE_COVERAGE_REQUIREMENT=$bamboo_codeCoverageRequirement
else
    export SFDX_CODE_COVERAGE_REQUIREMENT=90
fi

# Updating CLI if available
sfdx update

echo "Start CI run for user $bamboo_SFDXHubUserName buildkey $bamboo_buildKey resultKey $bamboo_buildResultKey"

# Auth
sfdx force:auth:jwt:grant --clientid $bamboo_devHubAppClientId --jwtkeyfile $bamboo_devHubKeyPath --username $bamboo_SFDXHubUserName --setdefaultdevhubusername --setalias $bamboo_devHubAlias
if [ $? -ne 0 ]
then
    echo "Failed authenticating"
    exit 1
fi

sfdx force:org:list

# Create scratch org
sfdx force:org:create -s -f $bamboo_scratchOrgDefinitionPath -a CIDevOrg-$bamboo_buildResultKey
if [ $? -ne 0 ]
then
    echo "Failed creating the scratch org"
    exit 1
fi

sfdx force:org:list

# Push source
sfdx force:source:push
if [ $? -ne 0 ]
then
    echo "Failed pushing the source to the scratch org"
    sfdx force:org:delete -u CIDevOrg-$bamboo_buildResultKey -p
    sfdx force:org:list
    exit 1
fi

# Run tests
if [ "$bamboo_configOnlyOrg" == "true" ] 
then
    echo "No tests run for this org. The build job is configured for this org to be config only (i.e. no code, therefore no tests). Please refer to the environment variables if this is not correct"
else
    if [ "$bamboo_namedTestClasses" ]
    then
        sfdx force:apex:test:run -c -n $bamboo_namedTestClasses -r junit -d testResults -w $bamboo_testWaitTimeout
    else
        if [ "$bamboo_testLevel" ] 
        then
            sfdx force:apex:test:run -c -l $bamboo_testLevel -r junit -d testResults -w $bamboo_testWaitTimeout
        else
            sfdx force:apex:test:run -c -l RunLocalTests -r junit -d testResults -w $bamboo_testWaitTimeout
        fi
    fi

    if [ $? -ne 0 ]
    then
        echo "Failed running tests"
        sfdx force:org:delete -u CIDevOrg-$bamboo_buildResultKey -p
        sfdx force:org:list
        exit 0
    fi
fi

# Delete and wrap-up
sfdx force:org:delete -u CIDevOrg-$bamboo_buildResultKey -p
if [ $? -ne 0 ]
then
    echo "Failed deleting the scratch org"
    exit 1
fi

sfdx force:org:list

exit 0