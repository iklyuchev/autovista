echo "Please enter alias:"
read alias

echo "Please enter the duration of the org:"
read duration

echo "Do you want to install roll-up helper? Enter 'Y' for yes 'N' for No."
read installRollupHelper

#create new default scratch org
echo "Creating scratch org with alias $alias and duration of $duration days"
sfdx force:org:create -f config/project-scratch-def.json -a $alias -d $duration -s
if [ $? -eq 1 ]; then
    exit
fi

#install dependant packages
echo ""
echo "Installing managed packages..."
echo ""

#install rollup-helper
if [ $installRollupHelper == 'Y' ]; then
echo "Installing Rollup helper from https://appexchange.salesforce.com/appxListingDetail?listingId=a0N30000009i3UpEAI"
sfdx force:package:install --package 04t0f000000kb9dAAA --noprompt -w 3
    if [ $? -eq 1 ]; then
        exit
    fi
fi

#setup currencies
sfdx force:data:tree:import -f scripts/currencyTypes.json

#push source to newly created scratch org
echo "Pushing source to scratch org..."
sfdx force:source:push
if [ $? -eq 1 ]; then
    exit
fi

#assign permission sets to default scratch org user
echo "Assigning Admin permission set to default scratch org user..."
sfdx force:user:permset:assign -n Admin_Permissions

#insert trigger custom setting
echo "Inserting trigger settings..."
sfdx force:apex:execute -f scripts/insertConfigSettings.apex