#!/bin/bash

# Don't exit on error
# TEST
set +e

codeCoverage=$(sfdx force:data:soql:query -q "SELECT PercentCovered FROM ApexOrgWideCoverage" -t -r json | jq -r .result.records[0].PercentCovered)

echo "Actual code coverage: $codeCoverage"

coverageRequirement=85

if [ "$bamboo_codeCoverageRequirement" ]
then
    coverageRequirement=$bamboo_codeCoverageRequirement 
fi

echo "Requirement is $coverageRequirement"

if [ $codeCoverage -lt $coverageRequirement ]
then
    echo "Too little coverage"
    exit 1
else
    echo "Code Covered satisfactorily"
fi