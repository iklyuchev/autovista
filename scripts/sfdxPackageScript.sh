#!/bin/bash

# Don't exit on error
set +e

# Updating CLI if available
sfdx update

# DevHub Auth
sfdx force:auth:jwt:grant --clientid $bamboo_devHubAppClientId --jwtkeyfile $bamboo_devHubKeyPath --username $bamboo_SFDXHubUserName --setdefaultdevhubusername --setalias $bamboo_devHubAlias
if [ $? -ne 0 ]
then
    echo "Failed authenticating"
    exit 1
fi

# Target Auth
sfdx force:auth:jwt:grant --clientid $bamboo_targetOrgClientId --jwtkeyfile $bamboo_targetOrgKeyPath --username $bamboo_targetOrgUserName --setalias $bamboo_targetOrgAlias -r $bamboo_targetOrgInstance --setdefaultusername
if [ $? -ne 0 ]
then
    echo "Failed authenticating"
    exit 1
fi


# List orgs and package versions before creating new package version
sfdx force:org:list
sfdx force:package2:version:list

# Create new package version, save the JSON output
versionCreateOutput=$(sfdx force:package2:version:create --directory force-app -w $bamboo_versionCreateTimeout --json)
if [ $? -ne 0 ]
then
    echo "Failed creating version"
    exit 1
fi

# Parse the JSON output and save the versionIds needed to install
echo "Version create output: $versionCreateOutput"
subscriberVersionId=$(echo $versionCreateOutput | grep -Eo '"SubscriberPackageVersionId":".*?[^\\]"' | cut -d':' -f2 | cut -d'"' -f 2)
package2VersionId=$(echo $versionCreateOutput | grep -Eo '"Package2VersionId":".*?[^\\]"' | cut -d':' -f2 | cut -d'"' -f 2)

# List orgs and package versions before installing new package version
echo "VersionId: $subscriberVersionId"
sfdx force:org:list
sfdx force:package2:version:list

# Marking the package version as released and installing it after waiting a specified period of time (if specified), to allow complete creation of the package
if [ "$bamboo_packageCreationSleepTime" ]
then
    echo "Sleeping for $bamboo_packageCreationSleepTime"
    sleep $bamboo_packageCreationSleepTime
fi

# Marking the package as released
sfdx force:package2:version:update -p -i $package2VersionId --setasreleased

# Installing the packeg in the target org
sfdx force:package:install -i $subscriberVersionId -w $bamboo_versionInstallTimeout
if [ $? -ne 0 ]
then
    echo "Failed installing version"
    exit 1
fi

exit 0